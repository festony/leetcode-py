#!/usr/bin/env python2
# https://leetcode.com/problems/dungeon-game/description/
from datetime import datetime
start = datetime.now()

class Solution(object):
    def calculateMinimumHP(self, dungeon):
        """
        :type dungeon: List[List[int]]
        :rtype: int
        """
        # solution 4 !!!! fast 100%!!!
        R = len(dungeon)
        C = len(dungeon[0])

        cache = {}
        def rec_find_lowest_hp_before(r, c):
            if (r, c) in cache:
                return cache[(r, c)]
            lowest_hp_after = -1
            if r == R-1 and c == C-1:
                lowest_hp_after = 1
            else:
                if r < R - 1:
                    lowest_hp_after = rec_find_lowest_hp_before(r+1, c)
                if c < C - 1:
                    hp_after = rec_find_lowest_hp_before(r, c+1)
                    if lowest_hp_after < 0 or hp_after < lowest_hp_after:
                        lowest_hp_after = hp_after
            if dungeon[r][c] >= lowest_hp_after - 1:
                cache[(r, c)] = 1
                return 1
            cache[(r, c)] = lowest_hp_after - dungeon[r][c]
            return lowest_hp_after - dungeon[r][c]

        return rec_find_lowest_hp_before(0, 0)




        # # solution 3 - again slightly optimization - 33.54%
        #
        # R = len(dungeon)
        # C = len(dungeon[0])
        #
        # cache = []
        # for i in range(R):
        #     cache.append([-1]*C)
        # cache[R-1][C-1] = 1
        #
        # for d in range(R+C-2, -1, -1):
        #     minr = 0
        #     if d > C-1:
        #         minr = d - C + 1
        #     maxr = R - 1
        #     if d < R - 1:
        #         maxr = d
        #
        #     for r in range(maxr, minr-1, -1):
        #         c = d - r
        #         v = dungeon[r][c]
        #         after_lowest = cache[r][c]
        #         if v >= after_lowest-1:
        #             before_lowest = 1
        #         else:
        #             before_lowest = after_lowest - v
        #         if c > 0:
        #             prev_r = r
        #             prev_c = c-1
        #             if cache[prev_r][prev_c] < 0 or cache[prev_r][prev_c] > before_lowest:
        #                 cache[prev_r][prev_c] = before_lowest
        #         if r > 0:
        #             prev_r = r-1
        #             prev_c = c
        #             if cache[prev_r][prev_c] < 0 or cache[prev_r][prev_c] > before_lowest:
        #                 cache[prev_r][prev_c] = before_lowest
        #         # print cache
        #
        # after_0_0 = cache[0][0]
        # v = dungeon[0][0]
        # if v >= after_0_0-1:
        #     before_0_0 = 1
        # else:
        #     before_0_0 = after_0_0 - v
        # return before_0_0




        # # solution 2 - try some optimization - little bit faster but still only 17.39%
        # R = len(dungeon)
        # C = len(dungeon[0])
        #
        # cache = {}
        # cache[(R-1, C-1)] = 1
        # for d in range(R+C-1)[::-1]:
        #     minr = 0
        #     if d > C-1:
        #         minr = d - C + 1
        #     maxr = R - 1
        #     if d < R - 1:
        #         maxr = d
        #
        #     for r in range(minr, maxr+1)[::-1]:
        #         c = d - r
        #         v = dungeon[r][c]
        #         after_lowest = cache[(r, c)]
        #         if v < 0:
        #             before_lowest = after_lowest - v
        #         elif v == 0:
        #             before_lowest = after_lowest
        #         else: # v > 0:
        #             if v >= after_lowest-1:
        #                 before_lowest = 1
        #             else:
        #                 before_lowest = after_lowest - v
        #         if c > 0:
        #             prev_r = r
        #             prev_c = c-1
        #             if (prev_r, prev_c) in cache:
        #                 if cache[(prev_r, prev_c)] > before_lowest:
        #                     cache[(prev_r, prev_c)] = before_lowest
        #             else:
        #                 cache[(prev_r, prev_c)] = before_lowest
        #         if r > 0:
        #             prev_r = r-1
        #             prev_c = c
        #             if (prev_r, prev_c) in cache:
        #                 if cache[(prev_r, prev_c)] > before_lowest:
        #                     cache[(prev_r, prev_c)] = before_lowest
        #             else:
        #                 cache[(prev_r, prev_c)] = before_lowest
        #         # print cache
        #
        # after_0_0 = cache[(0, 0)]
        # v = dungeon[0][0]
        # if v < 0:
        #     before_0_0 = after_0_0 - v
        # elif v == 0:
        #     before_0_0 = after_0_0
        # else: # v > 0:
        #     if v >= after_0_0-1:
        #         before_0_0 = 1
        #     else:
        #         before_0_0 = after_0_0 - v
        # return before_0_0
        #



        # # solution 1 - one try pass woohoo! yet bit slow, on 14.9%
        #
        # R = len(dungeon)
        # C = len(dungeon[0])
        #
        # cache = {}
        # cache[(R-1, C-1)] = [1]
        # for d in range(R+C-1)[::-1]:
        #     minr = 0
        #     if d > C-1:
        #         minr = d - C + 1
        #     maxr = R - 1
        #     if d < R - 1:
        #         maxr = d
        #
        #     for r in range(minr, maxr+1)[::-1]:
        #         c = d - r
        #         v = dungeon[r][c]
        #         after_lowest = min(cache[(r, c)])
        #         if v < 0:
        #             before_lowest = after_lowest - v
        #         elif v == 0:
        #             before_lowest = after_lowest
        #         else: # v > 0:
        #             if v >= after_lowest-1:
        #                 before_lowest = 1
        #             else:
        #                 before_lowest = after_lowest - v
        #         if c > 0:
        #             prev_r = r
        #             prev_c = c-1
        #             if (prev_r, prev_c) in cache:
        #                 cache[(prev_r, prev_c)].append(before_lowest)
        #             else:
        #                 cache[(prev_r, prev_c)] = [before_lowest]
        #         if r > 0:
        #             prev_r = r-1
        #             prev_c = c
        #             if (prev_r, prev_c) in cache:
        #                 cache[(prev_r, prev_c)].append(before_lowest)
        #             else:
        #                 cache[(prev_r, prev_c)] = [before_lowest]
        #         # print cache
        #
        # after_0_0 = min(cache[(0, 0)])
        # v = dungeon[0][0]
        # if v < 0:
        #     before_0_0 = after_0_0 - v
        # elif v == 0:
        #     before_0_0 = after_0_0
        # else: # v > 0:
        #     if v >= after_0_0-1:
        #         before_0_0 = 1
        #     else:
        #         before_0_0 = after_0_0 - v
        # return before_0_0





s = Solution()

print s.calculateMinimumHP([[-2, -3, 3], [-5, -10, 1], [10, 30, -5]]) # expect 7

print datetime.now()-start
