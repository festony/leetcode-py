#!/usr/bin/env python2
# https://leetcode.com/problems/longest-common-prefix/description/
from datetime import datetime
start=datetime.now()

class Solution(object):
    def longestCommonPrefix(self, strs):
        """
        :type strs: List[str]
        :rtype: str
        """
        if strs == []:
            return ''
        def comm_pref(str1, str2):
            comm = []
            for i in range(min(len(str1), len(str2))):
                if str1[i] == str2[i]:
                    comm.append(str1[i])
                else:
                    break
            return ''.join(comm)
        comm = strs[0]
        for i in range(len(strs) - 1):
            comm = comm_pref(comm, strs[i+1])
        return comm




s = Solution()


print datetime.now()-start
