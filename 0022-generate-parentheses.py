#!/usr/bin/env python2
# https://leetcode.com/problems/generate-parentheses/description/
from datetime import datetime
start = datetime.now()

class Solution(object):
    def generateParenthesis(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        # solution 1 recursive - 46ms/46.80%
        # def return_possible_next(still_open, total_open):
        #     if still_open == 0:
        #         return [['(', still_open + 1, total_open + 1]]
        #     if total_open == n:
        #         return [[')', still_open - 1, total_open]]
        #     return [['(', still_open + 1, total_open + 1], [')', still_open - 1, total_open]]
        # def gen_par(res):
        #     # if res[0][1] == 0 and res[0][2] == n:
        #     #     return res
        #     new_res = []
        #     for r in res:
        #         s = r[0]
        #         sr = return_possible_next(r[1], r[2])
        #         for subr in sr:
        #             n_s = s + subr[0]
        #             new_res.append([n_s, subr[1], subr[2]])
        #     return new_res
        # res = [['', 0, 0]]
        # while not (res[0][1] == 0 and res[0][2] == n):
        #     res = gen_par(res)
        # return map(lambda x: x[0], res)

        # solution 2 recursive - 42ms/66.70% - i suspect the running time is bit random
        cache = {}
        def gen_par(n):
            if n in cache:
                return cache[n]
            if n == 0:
                cache[0] = ['']
                return ['']
            if n == 1:
                cache[1] = ['()']
                return ['()']
            r = set([])
            rj = gen_par(n-1)
            for y in rj:
                r.add('(' + y + ')')
            for i in range(1, n):
                j = n - i
                ri = gen_par(i)
                rj = gen_par(j)
                for x in ri:
                    for y in rj:
                        r.add(x+y)
            r = list(r)
            cache[n] = r
            return r

        return gen_par(n)




s = Solution()

print s.generateParenthesis(3)

print datetime.now()-start
