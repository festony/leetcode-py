#!/usr/bin/env python2
# https://leetcode.com/problems/zigzag-conversion/description/
from datetime import datetime
start=datetime.now()

class Solution(object):
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        # solution 1 - with python it's like cheating ...
        if x > 2147483647 or x < -2147483648:
            return 0
        sign = 1
        if x < 0:
            sign = -1
            x *= -1
        r = int((str(x))[::-1]) * sign
        if r > 2147483648 or r < -2147483647:
            return 0
        return r

        # solution 2
        # get digits, reverse order, reconstruct int



s = Solution()
print s.reverse(123)

print datetime.now()-start
1534236469
2147483648
