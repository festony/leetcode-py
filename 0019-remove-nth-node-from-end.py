#!/usr/bin/env python2
# https://leetcode.com/problems/remove-nth-node-from-end-of-list/description/
from datetime import datetime
start = datetime.now()


# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def removeNthFromEnd(self, head, n):
        """
        :type head: ListNode
        :type n: int
        :rtype: ListNode
        """
        # solution 1 - straightforward - 52ms/16.25%
        # len_l = 1
        # p = head
        # while p.next != None:
        #     p = p.next
        #     len_l += 1
        # head_n = len_l - n
        # p = head
        # if head_n == 0:
        #     return p.next
        # for i in range(head_n - 1):
        #     p = p.next
        # p.next = p.next.next
        # return head

        # solution 2 - use stack - 42ms/52.28%
        # cache = [head]
        # p = head
        # while p.next != None:
        #     p = p.next
        #     cache.append(p)
        # to_remove = cache[-n]
        # if to_remove == head:
        #     return head.next
        # cache[-1-n].next = to_remove.next
        # return head

        # solution 3 - play trick, revert list first, remove nth node, then revert back - 38ms/88.85% (this is very weird, i think the time tracking for this puzzel is quite unreliable)
        # p0 = None
        # p1 = head
        # while p1 != None:
        #     t = p1.next
        #     p1.next = p0
        #     p0 = p1
        #     p1 = t
        # r_head = p0
        # if n == 1:
        #     r_head = r_head.next
        # else:
        #     for i in range(n-2):
        #         p0 = p0.next
        #     p0.next = p0.next.next
        # p0 = None
        # p1 = r_head
        # while p1 != None:
        #     t = p1.next
        #     p1.next = p0
        #     p0 = p1
        #     p1 = t
        # return p0

        # solution 4 - 2 pointers with n distance - 52ms/16.25% (again very weird)
        p0 = head
        p1 = head
        for i in range(n - 1):
            p1 = p1.next
        if p1.next == None:
            return head.next
        p1 = p1.next
        while p1.next != None:
            p0 = p0.next
            p1 = p1.next
        p0.next = p0.next.next
        return head











s = Solution()

# print s.removeNthFromEnd()

print datetime.now()-start
