#!/usr/bin/env python2
# https://leetcode.com/problems/letter-combinations-of-a-phone-number/description/
from datetime import datetime
start=datetime.now()

class Solution(object):
    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        # solution 1 - iterate all combinations, then print out - 48.73%
        # if digits == "":
        #     return []
        # dgs = map(int, list(digits))
        # dgs_lens = map(lambda x: 4 if x == 7 or x == 9 else 3, dgs)
        #
        # comb_iter = [0] * len(dgs)
        #
        # def inc():
        #     comb_iter[-1] += 1
        #     carry = 0
        #     for i in range(len(comb_iter))[::-1]:
        #         comb_iter[i] += carry
        #         if comb_iter[i] >= dgs_lens[i]:
        #             comb_iter[i] = 0
        #             carry = 1
        #         else:
        #             break
        #
        # letter_map = {}
        # letter_map[2] = "abc"
        # letter_map[3] = "def"
        # letter_map[4] = "ghi"
        # letter_map[5] = "jkl"
        # letter_map[6] = "mno"
        # letter_map[7] = "pqrs"
        # letter_map[8] = "tuv"
        # letter_map[9] = "wxyz"
        #
        # def map_comb():
        #     r = []
        #     for i in range(len(comb_iter)):
        #         r.append(letter_map[dgs[i]][comb_iter[i]])
        #     return ''.join(r)
        #
        # res = []
        # while True:
        #     res.append(map_comb())
        #     inc()
        #     if comb_iter == [0] * len(dgs):
        #         break
        # return res

        # solution 2 - recursive, 66.12%
        if digits == '':
            return []
        letter_map = {2: "abc", 3: "def", 4: "ghi", 5: "jkl", 6: "mno", 7: "pqrs", 8: "tuv", 9: "wxyz"}

        def get_all_comb(digits):
            if len(digits) == 1:
                return list(letter_map[int(digits)])
            else:
                res_rest = get_all_comb(digits[1:])
                res = []
                for c in letter_map[int(digits[0])]:
                    for rr in res_rest:
                        res.append(c+rr)
                return res
        return get_all_comb(digits)











s = Solution()

print s.letterCombinations("23")

print datetime.now()-start
