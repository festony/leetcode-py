#!/usr/bin/env python2
# https://leetcode.com/problems/longest-valid-parentheses/description/
from datetime import datetime
start = datetime.now()

class Solution(object):
    def longestValidParentheses(self, s):
        """
        :type s: str
        :rtype: int
        """
        # solution 1 - diffuse from () - 99ms / 17.25%
        # if s == '':
        #     return 0
        # first find all ()
        # i = 0
        # secs = []
        # while True:
        #     i = s.find('()', i)
        #     if i < 0:
        #         break
        #     secs.append([i, i+2])
        #     i += 2
        # modified = True
        # while modified:
        #     modified = False
        #     merging_indexes = set([])
        #     for i in range(len(secs) - 1):
        #         if secs[i][1] == secs[i+1][0]:
        #             merging_indexes.add(i)
        #             merging_indexes.add(i+1)
        #     merging_indexes = sorted(list(merging_indexes))

        # solution 2 - find most left '(' of each ')' - 79ms / used to 45.78% but now 37.68%
        # if s == '':
        #     return 0
        # cache = {}
        # def at_right_get_most_left(i):
        #     # only call when s[i] == ')'
        #     # returns the left most '(' index that between this index and i (inclusive) there is a valid parenthese str
        #     if i in cache:
        #         return cache[i]
        #     if i == 0:
        #         cache[i] = -1
        #         return -1
        #     if s[i-1] == '(':
        #         if i > 1 and s[i - 2] == ')' and at_right_get_most_left(i - 2) >= 0:
        #             r = at_right_get_most_left(i - 2)
        #             cache[i] = r
        #             return r
        #         else:
        #             cache[i] = i - 1
        #             return i - 1
        #     # else: s[i-1] == ')'
        #     prev_r = at_right_get_most_left(i-1)
        #     if prev_r <= 0:
        #         cache[i] = -1
        #         return -1
        #     if s[prev_r-1] == ')':
        #         cache[i] = -1
        #         return -1
        #     if prev_r == 1:
        #         cache[i] = prev_r - 1
        #         return prev_r - 1
        #     if s[prev_r - 2] == '(':
        #         cache[i] = prev_r - 1
        #         return prev_r - 1
        #     # s[prev_r - 2] == ')':
        #     pprev_r = at_right_get_most_left(prev_r - 2)
        #     if pprev_r < 0:
        #         cache[i] = prev_r - 1
        #         return prev_r - 1
        #     cache[i] = pprev_r
        #     return pprev_r
        #
        # longest = 0
        # for i in range(len(s)):
        #     if s[i] == ')':
        #         r = at_right_get_most_left(i)
        #         # print i, r
        #         if r >= 0 and i - r + 1 > longest:
        #             longest = i - r + 1
        # return longest

        # solution 3 - do counting - 74ms / 54.78%
        # if s == '':
        #     return 0
        # def get_secs(s, reverse=False):
        #     contl = []
        #     curr_l = 0
        #     curr_c = 0
        #     mark = '('
        #     if reverse:
        #         mark = ')'
        #     for i, c in enumerate(s):
        #         if c == mark:
        #             curr_c += 1
        #         else:
        #             curr_c -= 1
        #         if curr_c >= 0:
        #             curr_l += 1
        #             if curr_c == 0:
        #                 contl.append([i + 1 - curr_l, i + 1])
        #                 curr_l = 0
        #         else:
        #             curr_c = 0
        #             curr_l = 0
        #     rest = ''
        #     if curr_c != 0:
        #         rest = s[-curr_l:]
        #     return contl, rest
        # contl, rest = get_secs(s)
        # def get_max_l(contl):
        #     curr_p = 0
        #     curr_l = 0
        #     max_l = 0
        #     for st, ed in contl:
        #         l = ed - st
        #         if curr_p == st:
        #             l += curr_l
        #         curr_p = ed
        #         curr_l = l
        #         if l > max_l:
        #             max_l = l
        #     return max_l
        # max_l = get_max_l(contl)
        # if rest != '':
        #     contl, rest1 = get_secs(rest[::-1], True)
        # max_l1 = get_max_l(contl)
        # if max_l > max_l1:
        #     return max_l
        # return max_l1

        # fast solution fetched from sample code!!
        # thinking - self-stating data?
        max_len = 0
        stack = [-1]
        for i, c in enumerate(s):
            if c == '(':
                stack.append(i)
            else:
                stack.pop()
                if len(stack) == 0:
                    stack.append(i)
                else:
                    max_len = max(max_len, i - stack[-1])
        return max_len






s = Solution()

# print s.longestValidParentheses(')()())')
# print s.longestValidParentheses('()(())(((()))(()')
# print s.longestValidParentheses(')))((())))))(((((((())))))')
# print s.longestValidParentheses(')()())')
print s.longestValidParentheses(')(((())((())))')

print datetime.now()-start
