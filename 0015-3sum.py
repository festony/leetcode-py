#!/usr/bin/env python2
# https://leetcode.com/problems/3sum/description/
from datetime import datetime
start=datetime.now()

class Solution(object):
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        # Solution 1 - should be correct but using too much mem
        # pos = []
        # zeros = []
        # neg = []
        # for n in nums:
        #     if n > 0:
        #         pos.append(n)
        #     elif n == 0:
        #         zeros.append(0)
        #     else:
        #         neg.append(n)
        #
        # res = set([])
        # if len(zeros) > 2:
        #     res.add((0, 0, 0))
        # pos.sort()
        # neg.sort(reverse=True)
        #
        #
        # pos2 = {}
        # if len(pos) >= 2 and len(neg) >= 1:
        #     for i in range(len(pos) - 1):
        #         for j in range(i + 1, len(pos)):
        #             c = (min(pos[i], pos[j]), max(pos[i], pos[j]))
        #             s = pos[i] + pos[j]
        #             if s > -1*neg[-1]:
        #                 continue
        #             if s not in pos2:
        #                 pos2[s] = set([c])
        #             else:
        #                 pos2[s].add(c)
        # neg2 = {}
        # if len(pos) >= 1 and len(neg) >= 2:
        #     for i in range(len(neg) - 1):
        #         for j in range(i + 1, len(neg)):
        #             c = (min(neg[i], neg[j]), max(neg[i], neg[j]))
        #             s = neg[i] + neg[j]
        #             if s < -1*pos[-1]:
        #                 continue
        #             if s not in neg2:
        #                 neg2[s] = set([c])
        #             else:
        #                 neg2[s].add(c)
        #
        # pos2k = pos2.keys()
        # pos2k.sort()
        # neg2k = neg2.keys()
        # neg2k.sort(reverse=True)
        #
        # pos = list(set(pos))
        # pos.sort()
        # neg = list(set(neg))
        # neg.sort(reverse=True)
        #
        # # print pos, neg, pos2, neg2, pos2k, neg2k
        #
        # p_p = 0
        # p_n = 0
        # while p_p < len(pos) and p_n < len(neg2k):
        #     t = pos[p_p] + neg2k[p_n]
        #     if t == 0:
        #         for ng in neg2[neg2k[p_n]]:
        #             res.add((ng[0], ng[1], pos[p_p]))
        #         p_p += 1
        #         p_n += 1
        #     elif t < 0:
        #         p_p += 1
        #     else:
        #         p_n += 1
        #
        # p_p = 0
        # p_n = 0
        # while p_p < len(pos2k) and p_n < len(neg):
        #     t = neg[p_n] + pos2k[p_p]
        #     if t == 0:
        #         for ps in pos2[pos2k[p_p]]:
        #             res.add((neg[p_n], ps[0], ps[1]))
        #         p_p += 1
        #         p_n += 1
        #     elif t < 0:
        #         p_p += 1
        #     else:
        #         p_n += 1
        #
        # if zeros != []:
        #     p_p = 0
        #     p_n = 0
        #     while p_p< len(pos) and p_n < len(neg):
        #         t = neg[p_n] + pos[p_p]
        #         if t == 0:
        #             res.add((neg[p_n], 0, pos[p_p]))
        #             p_p += 1
        #             p_n += 1
        #         elif t < 0:
        #             p_p += 1
        #         else:
        #             p_n += 1
        # reslist = map(list, res)
        # return reslist

        # Solution 2 - bit improve space O, beats 98.50% =_=
        pos = []
        zeros = []
        neg = []
        for n in nums:
            if n > 0:
                pos.append(n)
            elif n == 0:
                zeros.append(0)
            else:
                neg.append(n)

        res = set([])
        if len(zeros) > 2:
            res.add((0, 0, 0))
        pos.sort()
        neg.sort(reverse=True)
        pos_s = set(pos)
        neg_s = set(neg)

        if len(pos) >= 2 and len(neg) >= 1:
            for i in range(len(pos) - 1):
                if -pos[i] in neg_s and zeros != []:
                    res.add((-pos[i], 0, pos[i]))
                for j in range(i + 1, len(pos)):
                    s = pos[i] + pos[j]
                    if -s in neg_s:
                        res.add((-s, min(pos[i], pos[j]), max(pos[i], pos[j])))

        if len(pos) >= 1 and len(neg) >= 1:
            if -pos[-1] in neg_s and zeros != []:
                res.add((-pos[-1], 0, pos[-1]))

        if len(neg) >= 2 and len(pos) >= 1:
            for i in range(len(neg) - 1):
                for j in range(i + 1, len(neg)):
                    s = neg[i] + neg[j]
                    if -s in pos_s:
                        res.add((min(neg[i], neg[j]), max(neg[i], neg[j]), -s))
        reslist = map(list, res)
        return reslist








s = Solution()

# print s.threeSum([-1, 0, 1, 2, -1, -4])

print s.threeSum([-4,-2,-2,-2,0,1,2,2,2,3,3,4,4,6,6])

print datetime.now()-start
