#!/usr/bin/env python2
# https://leetcode.com/problems/divide-two-integers/description/
from datetime import datetime
start = datetime.now()


class Solution(object):
    def divide(self, dividend, divisor):
        """
        :type dividend: int
        :type divisor: int
        :rtype: int
        """
        # solution 1 - straight forward - 55ms/35.01%
        MAX_INT = 2147483647
        if divisor == 0:
            return MAX_INT
        if dividend == 0:
            return 0
        if divisor == 1:
            r = dividend
            if r > MAX_INT:
                return MAX_INT
            if r < -1-MAX_INT:
                return -1-MAX_INT
            return r
        if divisor == -1:
            r = -dividend
            if r > MAX_INT:
                return MAX_INT
            if r < -1-MAX_INT:
                return -1-MAX_INT
            return r
        sg = 1
        if (divisor < 0 and dividend > 0) or (divisor > 0 and dividend < 0):
            sg = -1
        if divisor < 0:
            divisor *= -1
        if dividend < 0:
            dividend *= -1
        cache = {}
        cache[1] = divisor
        i = 1
        d = divisor
        while d < dividend:
            i += i
            d += d
            cache[i] = d
        if d == dividend:
            return i * sg
        quo = 0
        dd = dividend
        sk = sorted(cache.keys())
        def get_max_divisor(dd):
            prev = 0
            for k in sk:
                if cache[k] == dd:
                    return k
                if cache[k] > dd:
                    return prev
                prev = k

        while dd >= divisor:
            td = get_max_divisor(dd)
            print dd, td
            quo += td
            dd -= cache[td]
        r = quo
        if sg < 0:
            r = -r
        if r > MAX_INT:
            return MAX_INT
        if r < -1-MAX_INT:
            return -1-MAX_INT
        return r








s = Solution()

print s.divide(2147483647, 2)

print datetime.now()-start
