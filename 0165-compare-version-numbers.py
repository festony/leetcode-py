#!/usr/bin/env python2
# https://leetcode.com/problems/longest-valid-parentheses/description/
from datetime import datetime
start = datetime.now()

class Solution(object):
    def compareVersion(self, version1, version2):
        """
        :type version1: str
        :type version2: str
        :rtype: int
        """
        # thinking - edge cases! e.g. '1.0' vs '1'
        v1 = map(int, version1.split('.'))
        v2 = map(int, version2.split('.'))
        if v1 == v2:
            return 0
        if len(v1) > len(v2):
            v2 += [0] * (len(v1) - len(v2))
        elif len(v1) < len(v2):
            v1 += [0] * (len(v2) - len(v1))
        for i in range(len(v1)):
            if v1[i] > v2[i]:
                return 1
            if v1[i] < v2[i]:
                return -1
        return 0

s = Solution()

print s.compareVersion('1', '1.0')

print datetime.now()-start
