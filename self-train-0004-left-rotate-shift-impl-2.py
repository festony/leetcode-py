# from programming pearls chap 2 - left rotate shift a string of length n by i
# e.g. for length n=8 string abcdefgh, left rotate shift for i=3 chars, we'll get: defghabc
# this is the second implementation(amazing thinking!):
# say for shifting length n=8 string abcdefgh for i=3 chars:
# abc|defgh => flap first section and second section => cba|hgfed => flap whole
# => defgh|abc
# DONE!
# A|B => B|A:
# B|A = (A^|B^)^, ^ means flap

class Solution(object):
    def LeftRotateShift(self, s, i):
        """
        :type s: str
        :type i: int
        Left rotate shift string s for i chars.
        :rtype: str
        """
        def flap(x):
            r = list(x)
            for i in range(len(r)/2):
                r[i], r[len(r)-i-1] = r[len(r)-i-1], r[i]
            return ''.join(r)

        return flap(flap(s[:i])+flap(s[i:]))

s = Solution()
print s.LeftRotateShift('abcdefgh', 1)
print s.LeftRotateShift('abcdefgh', 2)
print s.LeftRotateShift('abcdefgh', 3)
print s.LeftRotateShift('abcdefgh', 4)
print s.LeftRotateShift('abcdefgh', 5)
print s.LeftRotateShift('abcdefgh', 6)
print s.LeftRotateShift('abcdefgh', 7)
