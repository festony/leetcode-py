#!/usr/bin/env python2
# https://leetcode.com/problems/valid-parentheses/description/
from datetime import datetime
start = datetime.now()


class Solution(object):
    def isValid(self, s):
        """
        :type s: str
        :rtype: bool
        """
        # solution 1 - straightforward, 42ms/39.72%
        if s == '':
            return True
        cache = []

        def read(c):
            if c == '(' or c == '[' or c == '{':
                cache.append(c)
                return True
            if not cache:
                return False
            curr = cache.pop(-1)
            if c == ')':
                return curr == '('
            if c == ']':
                return curr == '['
            if c == '}':
                return curr == '{'
            # or True, if ignoring all other chars
            return False
        for c in s:
            if not read(c):
                return False
        return cache == []










s = Solution()

print s.isValid('[{{}}]')
print s.isValid('[(])')
print s.isValid(')[](')
print s.isValid('({}{{{[[][[[]]]]}}})')

print datetime.now()-start
