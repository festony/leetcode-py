#!/usr/bin/env python2
# https://leetcode.com/problems/reverse-nodes-in-k-group/description/
from datetime import datetime
start = datetime.now()


# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def reverseKGroup(self, head, k):
        """
        :type head: ListNode
        :type k: int
        :rtype: ListNode
        """
        # solution 1 straightforward - 55ms/99.22%
        if head is None or head.next is None or k == 1:
            return head
        prev = None
        h1 = None
        p = head
        while p is not None:
            pE = p
            for i in range(k-1):
                pE = pE.next
                if pE is None:
                    if h1 is not None:
                        return h1
                    return head
            pN = pE.next
            if prev is not None:
                prev.next = pE
            else:
                h1 = pE
            prev = p
            p1 = p
            p2 = p1.next
            while p1 != pE:
                t = p2.next
                p2.next = p1
                p1 = p2
                p2 = t

            p.next = pN
            p = pN
        return h1

s = Solution()

# print s.reverseKGroup()

print datetime.now()-start
