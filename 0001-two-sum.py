#!/usr/bin/env python2
# https://leetcode.com/problems/two-sum/description/

class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """

        # solution 1: bit slow!
        # makeup = map(lambda x:target - x, nums)
        # for i in range(len(makeup)):
        #     if makeup[i] in nums:
        #         return [i, nums.index(makeup[i])]
        # # error!
        # return [-1, -1]

        # solution 2: still too slow!
        # for i in range(len(nums)):
        #     rev = target - nums[i]
        #     for j in range(i+1, len(nums)):
        #         if rev == nums[j]:
        #             return [i, j]
        # # error!
        # return [-1, -1]

        # solution 3: faster! hit ~73%
        # cache = {}
        # for i in range(len(nums)):
        #     rev = target - nums[i]
        #     if cache.has_key(rev):
        #         return cache[rev], i
        #     cache[nums[i]] = i
        # # error!
        # return [-1, -1]

        # solution 4: tiny improvement? use enumerate, bump to 90.15%!
        cache = {}
        for i, n in enumerate(nums):
            rev = target - n
            if cache.has_key(rev):
                return cache[rev], i
            cache[n] = i
        # error!
        return [-1, -1]
