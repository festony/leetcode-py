#!/usr/bin/env python2
# https://leetcode.com/problems/swap-nodes-in-pairs/description/
from datetime import datetime
start = datetime.now()


# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def swapPairs(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        # solution 1 - straightforward - 29ms/97.08%
        i = 0
        p = head
        if p is None:
            return p
        p1 = p.next
        if p1 is None:
            return p
        head = p1
        prev = None
        while p is not None and p1 is not None:
            if prev is not None:
                prev.next = p1
            t = p1.next
            p1.next = p
            p.next = t
            prev = p
            if t is None:
                break
            p = t
            if p is None:
                break
            if p.next is None:
                break
            p1 = p.next
        return head

s = Solution()

# print s.mergeKLists()

print datetime.now()-start
