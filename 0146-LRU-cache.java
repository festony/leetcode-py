// https://leetcode.com/problems/lru-cache/description/


// Solution 1 - 157ms / 33.26%

class LRUCache {

    class CacheNode {
        int key;
        int value;
        CacheNode prev;
        CacheNode next;
    }

    class CacheLinkedList {
        int capacity;
        CacheNode head;
        CacheNode tail;
        int size;
        Deque<CacheNode> nodeBuffer;
        HashMap<Integer, CacheNode> keyMap;

        CacheLinkedList(int capacity) {
            this.capacity = capacity;
            CacheNode head = null;
            CacheNode tail = null;
            this.size = 0;
            nodeBuffer = new LinkedList<CacheNode>();
            for (int i=0; i<capacity; i++) {
                nodeBuffer.push(new CacheNode());
            }
            keyMap = new HashMap<>(capacity);
        }

        // must and must only call when capacity is reached
        void deleteHead() {
            keyMap.remove(head.key);
            CacheNode temp = head;
            head = head.next;
            temp.prev = null;
            temp.next = null;
            nodeBuffer.push(temp);
            size --;
            if (size == 0) {
                head = null;
                tail = null;
            }
        }

        void promptNodeToTail(CacheNode node) {
            if (tail == node) {
                return;
            }
            if (head == node) {
                head = node.next;
                head.prev = null;
            } else {
                CacheNode currPrev = node.prev;
                currPrev.next = node.next;
                node.next.prev = currPrev;
            }
            tail.next = node;
            node.prev = tail;
            node.next = null;
            tail = node;
        }

        // must make sure k is new
        CacheNode pushNode(int k, int v) {
            if (nodeBuffer.isEmpty()) {
                deleteHead();
            }

            CacheNode newNode = nodeBuffer.pop();
            newNode.key = k;
            newNode.value = v;

            if (tail == null) {
                head = newNode;
                tail = newNode;
            } else {
                tail.next = newNode;
                newNode.prev = tail;
                tail = newNode;
            }

            keyMap.put(k, newNode);
            size ++;
            return newNode;
        }

        // must make sure k exist already
        CacheNode updateNode(int k, int v) {
            CacheNode node = keyMap.get(k);
            node.value = v;
            promptNodeToTail(node);
            return node;
        }

        void put(int k, int v) {
            if (keyMap.containsKey(k)) {
                updateNode(k, v);
            } else {
                pushNode(k, v);
            }
        }

        int get(int k) {
            if (keyMap.containsKey(k)) {
                CacheNode node = keyMap.get(k);
                promptNodeToTail(node);
                return node.value;
            } else {
                return -1;
            }
        }
    }

    private CacheLinkedList list;

    public LRUCache(int capacity) {
        list = new CacheLinkedList(capacity);
    }

    public int get(int key) {
        return list.get(key);
    }

    public void put(int key, int value) {
        list.put(key, value);
    }
}
