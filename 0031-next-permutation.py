#!/usr/bin/env python2
# https://leetcode.com/problems/next-permutation/description/
from datetime import datetime
start = datetime.now()


class Solution(object):
    def nextPermutation(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        # solution 1 - 65ms/19.88%
        if len(nums) < 2:
            return

        def swap(i, j):
            nums[i], nums[j] = nums[j], nums[i]

        def flap(i, j):
            if j - i <= 1:
                return
            for k in range((j - i)/2):
                swap(i + k, j - k - 1)

        def right_rotate_shift(start, end, k):
            # end index exclusive
            flap(start, end-k)
            flap(end-k, end)
            flap(start, end)

        def left_rotate_shift(start, end, k):
            # end index exclusive
            flap(start, start + k)
            flap(start + k, end)
            flap(start, end)

        for i in range(len(nums)-1)[::-1]:
            if nums[i] < nums[i+1]:
                j = i + 1
                while j < len(nums) - 1 and nums[j+1] > nums[i]:
                    j += 1
                swap(i, j)
                # now need to make elements after position i sorted in ascending order
                # but except position j, other elements are already in descending order
                # so need to get j into right position then flap the section
                # namaely: find first (from left to right) element that is smaller than or equal to nums[j]
                # given previous nums[j], now nums[i] is always larger than curr nums[j], the element we are
                # looking for must fall in right half of j.
                # then put nums[j] at left side of it, and then flap whole area after position i.
                # then it is find the left most element in [i+1, len(nums)) range of nums array, try use binary search:
                up_index = len(nums)
                down_index = i + 1
                while up_index - down_index > 1:
                    mid_index = (up_index + down_index) / 2
                    if nums[mid_index] == nums[j]:
                        up_index = mid_index + 1
                        break
                    if nums[mid_index] > nums[j]:
                        down_index = mid_index
                        continue
                    if nums[mid_index] < nums[j]:
                        up_index = mid_index
                left_rotate_shift(j, up_index, 1)
                flap(i+1, len(nums))
                print nums
                return
        flap(0, len(nums))
        print nums
        return




s = Solution()

print s.nextPermutation([1, 2, 3])
print s.nextPermutation([1, 2, 3, 3])
print s.nextPermutation([1, 3, 2])
print s.nextPermutation([2, 4, 3, 1])

print datetime.now()-start
