#!/usr/bin/env python2
# https://en.wikipedia.org/wiki/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm

class Solution(object):
    def KMP(self, s, w):
        """
        :type s: str
        :type w: str
        Search for target string w in s.
        :rtype: str
        """
        # first LPS generating method: not so efficient
        # offsets = [0]
        # for i in range(len(w)):
        #     o = i+1
        #     if i > 1:
        #         for j in range(1, i+1):
        #             teststr1 = w[:i+1-j]
        #             teststr2 = w[j:i+1]
        #             if teststr1 == teststr2:
        #                 o = j
        #                 break
        #     offsets.append(o)
        # print offsets

        # second LPS generating method, which uses similar logic in main search.
        offsets = range(len(w) + 1)
        o = 1
        i = 0
        while i+o < len(w):
            if w[i] == w[i+o]:
                offsets[i+o+1] = o
                i += 1
            else:
                if i == 0:
                    o += 1
                else:
                    o += offsets[i]
                    i -= offsets[i]
        # print offsets

        m = 0
        i = 0
        while m < len(s):
            if s[m] == w[i]:
                m += 1
                i += 1
                if i == len(w):
                    print 'match! at', m - len(w)
                    # here not skipping the matched part
                    i -= offsets[i]
            else:
                if i == 0:
                    m += 1
                i -= offsets[i]
        print 'matching completed!'

s = Solution()

s.KMP('fjdskfjfdjafkfjddjalfjdskfjdalaf', 'fjdskfjda')
s.KMP('ABC ABCDAB ABCDABCDABDE', 'ABCDABD')
s.KMP('ABC ABCDAB ABCDABCDABDE', 'AB')
s.KMP('ABC ABCDAB ABCDABCDABDE', 'ABC')
s.KMP('ABC ABCDAB ABCDABCDABDE', 'AAB')
s.KMP('ABC ABCDAB ABCDABCDABDE', 'AAAA')
s.KMP('ABC ABCDAB ABCDABCDABDE', 'ABAB')
s.KMP('ABC ABCDAB ABCDABCDABDE', 'ABABC')
s.KMP('ABC ABCDAB ABCDABCDABDE', 'ABCAB')



#
# ABCDAB
# 0 1 2 3 4 5
# -o--
#     ABCDAB
# ABCDAB
#
# AACAACDAACD
#    AACA
