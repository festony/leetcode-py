#!/usr/bin/env python2
# https://leetcode.com/problems/longest-substring-without-repeating-characters/description/

class Solution(object):
    def lengthOfLongestSubstring(self, s):
        """
        :type s: str
        :rtype: int
        """
        # solution 1: too slow!
        # curr_len = 0
        # count_start = 0
        # last_index = {}
        # curr_chars = set([])
        # longest = 0
        # subs = ''
        # for i, c in enumerate(s):
        #     if c in curr_chars:
        #         curr_len = i - count_start
        #         if curr_len > longest:
        #             longest = curr_len
        #         count_start = last_index[c] + 1
        #         curr_chars = set(list(s[count_start:i+1]))
        #     else:
        #         curr_chars.add(c)
        #     last_index[c] = i
        # curr_len = len(s) - count_start
        # if curr_len > longest:
        #     longest = curr_len
        # return longest

        # solution 2 much faster! hit 99.07%. same logic just removed some redundant code and changed condition check a little bit.
        longest = 0
        last_index = {}
        count_start = 0
        for i, c in enumerate(s):
            if c in last_index and last_index[c] >= count_start:
                curr_len = i - count_start
                if curr_len > longest:
                    longest = curr_len
                count_start = last_index[c] + 1
            last_index[c] = i
        curr_len = len(s) - count_start
        if curr_len > longest:
            longest = curr_len
        return longest
