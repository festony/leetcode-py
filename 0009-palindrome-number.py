#!/usr/bin/env python2
# https://leetcode.com/problems/palindrome-number/description/
from datetime import datetime
start=datetime.now()

class Solution(object):
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        # solution 1 straightforward
        # if x < 0:
        #     return False
        # if x < 10:
        #     return True
        # if x % 10 == 0:
        #     return False
        # if x >= 1000000000:
        #     digit_len = 10
        # else:
        #     i = 0
        #     b = 1
        #     while b < x:
        #         b *= 10
        #         i += 1
        #     digit_len = i
        # def get_digit(n, d):
        #     if d >= 9:
        #         return n / 1000000000
        #     b = 1
        #     bl = 10
        #     for i in range(d):
        #         b *= 10
        #         bl *= 10
        #     return (n%bl) / b
        # for i in range(digit_len / 2):
        #     if get_digit(x, i) != get_digit(x, digit_len - 1 - i):
        #         return False
        # return True

        # solution 2 - right direction but still why always need to extract digits? introduces more complexity
        # if x < 0:
        #     return False
        # if x < 10:
        #     return True
        # if x % 10 == 0:
        #     return False
        # if x >= 1000000000:
        #     digit_len = 10
        # else:
        #     i = 0
        #     b = 1
        #     while b < x:
        #         b *= 10
        #         i += 1
        #     digit_len = i
        # half_len = digit_len / 2
        # b = 1
        # for i in range(half_len):
        #     b *= 10
        # if digit_len % 2 == 0:
        #     prev_half = x / b
        #     next_half = x % b
        # else:
        #     prev_half = x / (b*10)
        #     next_half = x % b
        # print prev_half, next_half, b
        # b /= 10
        # for i in range(half_len):
        #     p_d = prev_half / b
        #     n_d = next_half % 10
        #     if p_d != n_d:
        #         return False
        #     prev_half %= b
        #     b /= 10
        #     next_half /= 10
        # return True

        # solution 3 - suggested solution
        if x < 0:
            return False
        if x < 10:
            return True
        if x % 10 == 0:
            return False
        rx = 0
        while True:
            d = x % 10
            rx *= 10
            rx += d
            if rx == x:
                return True
            if rx > x:
                return False
            x /= 10
            if rx == x:
                return True
            if rx > x:
                return False




s = Solution()
print s.isPalindrome(12321)
print s.isPalindrome(5)

print datetime.now()-start
