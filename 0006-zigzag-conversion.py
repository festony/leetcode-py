#!/usr/bin/env python2
# https://leetcode.com/problems/zigzag-conversion/description/
from datetime import datetime
start=datetime.now()

class Solution(object):
    def convert(self, s, numRows):
        """
        :type s: str
        :type numRows: int
        :rtype: str
        """
        # solution 1 - staightforward, bit slow 17.75%
        # if numRows == 1:
        #     return s
        # rows = {}
        # for i in range(numRows):
        #     rows[i] = []
        # r = 0
        # offset = 1
        # for c in s:
        #     rows[r].append(c)
        #     r += offset
        #     if r == 0 or r == numRows - 1:
        #         offset = - offset
        # # print rows
        # res = ''
        # for i in rows.keys():
        #     res += ''.join(rows[i])
        # return res

        # solution 2 - bit faster - 64.39%
        if numRows == 1:
            return s
        res = []
        for i in range(numRows):
            if i == 0 or i == numRows - 1:
                j = i
                while j < len(s):
                    res.append(s[j])
                    j += 2 * numRows - 2
            else:
                alt_offset = [(numRows - i - 1) * 2, numRows * 2 - 2 - (numRows - i - 1) * 2]
                off_i = 0
                j = i
                while j < len(s):
                    res.append(s[j])
                    j += alt_offset[off_i]
                    off_i = 1 - off_i
        return ''.join(res)



s = Solution()
print s.convert('AB', 1)
print s.convert('PAYPALISHIRING', 3)

print datetime.now()-start
