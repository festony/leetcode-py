#!/usr/bin/env python2
# https://leetcode.com/problems/remove-duplicates-from-sorted-array/description/
from datetime import datetime
start = datetime.now()


class Solution(object):
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        # solution 1 - should work but time exceeded.
        # def find_first_repeat(start_i, end_i):
        #     r_start = -1
        #     for i in range(start_i, end_i - 1):
        #         if r_start < 0 and nums[i] == nums[i+1]:
        #             r_start = i
        #             continue
        #         if r_start >= 0 and nums[i] != nums[i+1]:
        #             return [r_start, i]
        #     if r_start >= 0:
        #         return [r_start, end_i - 1]
        #     return []
        #
        # def flap(start_i, end_i):
        #     # end_i: exclusive
        #     half_l = (end_i - start_i) / 2
        #     for i in range(half_l):
        #         nums[start_i+i], nums[end_i-1-i] = nums[end_i-1-i], nums[start_i+i]
        #
        # def left_rotate_shift(start_i, end_i, n):
        #     # end_i: exclusive
        #     # left rotate shift for n elems
        #     flap(start_i, start_i + n)
        #     flap(start_i + n, end_i)
        #     flap(start_i, end_i)
        #
        # end_area = 0
        # start_i = 0
        # while True:
        #     r_area = find_first_repeat(start_i, len(nums) - end_area)
        #     if r_area == []:
        #         break
        #     n = r_area[1] - r_area[0]
        #     left_rotate_shift(r_area[0], len(nums) - end_area, n)
        #     start_i = r_area[0] + 1
        #     end_area += n
        # return len(nums) - end_area

        # solution 2 - faster - 112ms/23.10%
        # if len(nums) < 2:
        #     return len(nums)
        #
        # self.p1 = 1
        # self.p2 = 1
        #
        # def one_step():
        #     if self.p1 != self.p2:
        #         nums[self.p1], nums[self.p2] = nums[self.p2], nums[self.p1]
        #     self.p1 += 1
        #     self.p2 += 1
        #
        # while self.p2 < len(nums):
        #     one_step()
        #     if nums[self.p1 - 1] == nums[self.p1 - 2]:
        #         self.p1 -= 1
        # return self.p1

        # solution 3 - similar idea with 2 but bit optimized - 82ms/59.97%
        if len(nums) < 2:
            return len(nums)
        p1 = 0
        p2 = 0

        while p2 < len(nums) - 1:
            if nums[p2] == nums[p2+1]:
                p2 += 1
                continue
            nums[p1], nums[p2] = nums[p2], nums[p1]
            p1 += 1
            p2 += 1
        nums[p1], nums[p2] = nums[p2], nums[p1]
        p1 += 1
        return p1

s = Solution()

print s.removeDuplicates([1, 1, 2])

print datetime.now()-start
