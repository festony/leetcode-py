#!/usr/bin/env python2
# https://leetcode.com/problems/merge-two-sorted-lists/description/
from datetime import datetime
start = datetime.now()

# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def mergeTwoLists(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        if l1 is None:
            return l2
        if l2 is None:
            return l1
        p1 = l1
        p2 = l2
        if p1.val <= p2.val:
            head = p1
            p1 = p1.next
        else:
            head = p2
            p2 = p2.next
        p = head
        while p1 is not None or p2 is not None:
            if p2 is None or (p1 is not None and p1.val < p2.val):
                p.next = p1
                p = p1
                p1 = p1.next
            else:
                p.next = p2
                p = p2
                p2 = p2.next
        return head














s = Solution()

# print s.mergeTwoLists()

print datetime.now()-start
