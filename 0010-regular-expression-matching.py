#!/usr/bin/env python2
# https://leetcode.com/problems/regular-expression-matching/description/
from datetime import datetime
start=datetime.now()

class Solution(object):
    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        # Solution 1 - divide & conquer
        # if '*' not in p:
        #     # easy case
        #     if len(s) != len(p):
        #         return False
        #     for i in range(len(s)):
        #         if p[i] != '.' and p[i] != s[i]:
        #             return False
        #     return True
        # # now hard case: with wildcard *
        #
        # # split pattern into sections
        # p_secs = []
        # for i in range(len(p)):
        #     if i < len(p) - 1 and p[i+1] == '*':
        #         continue
        #     if p[i] != '*':
        #         p_secs.append(p[i])
        #     else:
        #         p_secs.append(p[i-1:i+1])
        # # print p_secs
        #
        # # and obviously, if same pattern section with '*' appear in sequence, they can be merged
        # # e.g. 'a*a*' === 'a*'; '.*.*.*' === '.*'
        # proc_secs = []
        # for sec in p_secs:
        #     if proc_secs != [] and (len(sec) > 1 and sec == proc_secs[-1]):
        #         continue
        #     proc_secs.append(sec)
        # # print proc_secs
        #
        # def tryMatch(s, p_secs):
        #     if p_secs == []:
        #         # empty pattern only matches empty string
        #         return s == ''
        #     if '*' not in p_secs[0]:
        #         # if first sections are fixed ones: match them first
        #         if len(s) > 0 and (p_secs[0][0] == '.' or p_secs[0][0] == s[0]):
        #             return tryMatch(s[1:], p_secs[1:])
        #         return False
        #     if '*' not in p_secs[-1]:
        #         # if last sections are fixed ones: match them first
        #         if len(s) > 0 and (p_secs[-1][0] == '.' or p_secs[-1][0] == s[-1]):
        #             return tryMatch(s[:-1], p_secs[:-1])
        #         return False
        #     # now find first fixed section in pattern sections, find possible matches in s, and split the s and the pattern sections into two parts, match them separately.
        #     # divide & conquer
        #     foundFixSec = False
        #     firstNotFixI = -1
        #     firstNotFixSec = ''
        #     for i, sec in enumerate(p_secs):
        #         if '*' not in sec:
        #             firstNotFixI = i
        #             firstNotFixSec = sec
        #             foundFixSec = True
        #             break
        #     if foundFixSec:
        #         # notice even for fixed section matching, the pattern could be '.'
        #         if firstNotFixSec != '.':
        #             s_split_index = -1
        #             while True:
        #                 s_split_index = s.find(firstNotFixSec, s_split_index + 1)
        #                 if s_split_index < 0:
        #                     return False
        #                 s_1 = s[:s_split_index]
        #                 p_secs_1 = p_secs[:firstNotFixI]
        #                 s_2 = s[s_split_index + 1:]
        #                 p_secs_2 = p_secs[firstNotFixI + 1:]
        #                 if tryMatch(s_1, p_secs_1) and tryMatch(s_2, p_secs_2):
        #                     return True
        #         else:
        #             # EDGE CASE ---- '.'!!!
        #             # any char would match
        #             for s_split_index in range(len(s)):
        #                 s_1 = s[:s_split_index]
        #                 p_secs_1 = p_secs[:firstNotFixI]
        #                 s_2 = s[s_split_index + 1:]
        #                 p_secs_2 = p_secs[firstNotFixI + 1:]
        #                 if tryMatch(s_1, p_secs_1) and tryMatch(s_2, p_secs_2):
        #                     return True
        #             return False
        #     else:
        #         # all with *
        #         if s == '':
        #             return True
        #         for sec in p_secs:
        #             if sec[0] == '.':
        #                 # contains .* - always match any string
        #                 return True
        #         # now there's no '.*' in sections - just consume the first pattern section then match the rest.
        #         if s[0] != p_secs[0][0]:
        #             # which means '*' == 0, lol
        #             return tryMatch(s, p_secs[1:])
        #         firstNotMatchSec0I = 0
        #         while firstNotMatchSec0I < len(s) and s[firstNotMatchSec0I] == p_secs[0][0]:
        #             firstNotMatchSec0I += 1
        #         if firstNotMatchSec0I == len(s):
        #             return True
        #         return tryMatch(s[firstNotMatchSec0I:], p_secs[1:])
        #
        # return tryMatch(s, proc_secs)

        # Solution 2 - try DP!
        cache = {}
        def tryMatch(s_i, p_i):
            if (s_i, p_i) in cache:
                return cache[(s_i, p_i)]
            if s_i == len(s):
                if p_i < len(p) - 1 and p[p_i+1] == '*':
                    r = tryMatch(s_i, p_i + 2)
                    cache[(s_i, p_i)] = r
                    return r
                r = p_i == len(p)
                cache[(s_i, p_i)] = r
                return r
            if p_i == len(p):
                r = s_i == len(s)
                cache[(s_i, p_i)] = r
                return r
            first_char_matched = s[s_i] == p[p_i] or p[p_i] == '.'
            if p_i < len(p) - 1 and p[p_i+1] == '*':
                r = tryMatch(s_i, p_i + 2) or (first_char_matched and tryMatch(s_i + 1, p_i))
                cache[(s_i, p_i)] = r
                return r
            else:
                r = first_char_matched and tryMatch(s_i + 1, p_i + 1)
                cache[(s_i, p_i)] = r
                return r
        return tryMatch(0, 0)










s = Solution()
# print s.isMatch("aa","aa")
# print s.isMatch("aaa","aa")
# print s.isMatch("aa","aaa")
# print s.isMatch("aab", "c*a*b")
# print s.isMatch("aab", "c*a*a*.*.*b")

print s.isMatch("ab",".*..c*")
print s.isMatch("aa","a")

print datetime.now()-start
