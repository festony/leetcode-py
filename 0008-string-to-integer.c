// https://leetcode.com/problems/string-to-integer-atoi/description/

int myAtoi(char* str) {
    int i = 0;

    while (str[i] != '\0' && str[i] == ' ') {
        i ++;
    }

    if (str[i] == '\0' || ((str[i] < '0' || str[i] > '9') && str[i] != '+' && str[i] != '-')) {
        return 0;
    }

    int sign = 1;
    if (str[i] == '-') {
        sign = -1;
        i ++;
    } else if (str[i] == '+') {
        i ++;
    }

    if (str[i] == '\0' || (str[i] < '0' || str[i] > '9')) {
        return 0;
    }

    while (str[i] == '0') {
        i ++;
    }

    if (str[i] == '\0' || (str[i] < '0' || str[i] > '9')) {
        return 0;
    }

    int start = i;
    int end = i;
    while (str[end] != '\0' && str[end] <= '9' && str[end] >= '0') {
        end ++;
    }

    int r = 0;
    int max_v = 2147483647;
    int min_v = -2147483648;
    // printf("%d %d", start, end);
    for (i=start; i<end; i++) {
        if (sign == 1 && r > max_v / 10) {
            return max_v;
        }
        if (sign == -1 && r < min_v / 10) {
            return min_v;
        }
        r *= 10;
        int d = (str[i] - '0') * sign;
        // printf("%d", d);
        if (sign == 1 && r > max_v - d) {
            return max_v;
        }
        if (sign == -1 && r < min_v - d) {
            return min_v;
        }
        r += d;
    }
    return r;
}
