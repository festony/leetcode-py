#!/usr/bin/env python2
# https://leetcode.com/problems/median-of-two-sorted-arrays/description/

class Solution(object):
    def findMedianSortedArrays(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: float
        """
        # solution 1 classic - complete merge sorting then get median, have a feeling it can be done by binary search gonna try it in solution 2. Divide & Conquer!
        # slow! hit 62.43%
        # nums = []
        # p1 = 0
        # p2 = 0
        # while p1 < len(nums1) or p2 < len(nums2):
        #     if p1 >= len(nums1):
        #         nums.append(nums2[p2])
        #         p2 += 1
        #         continue
        #     if p2 >= len(nums2):
        #         nums.append(nums1[p1])
        #         p1 += 1
        #         continue
        #     if nums1[p1] < nums2[p2]:
        #         nums.append(nums1[p1])
        #         p1 += 1
        #     else:
        #         nums.append(nums2[p2])
        #         p2 += 1
        # if len(nums) % 2 == 1:
        #     return float(nums[len(nums)/2])
        # return (nums[len(nums)/2-1] + nums[len(nums)/2]) / 2.0

        # solution 2 - binary search!
        # fxxking too long, not graceful at all, also only hit 65.42%
        # special cases: simply connect 2 arrays and it's already sorted
        # if nums1 == [] and nums2 == []:
        #     # error i think
        #     return 0
        # if nums1 == []:
        #     if len(nums2) % 2 == 1:
        #         return float(nums2[len(nums2)/2])
        #     return (nums2[len(nums2)/2-1] + nums2[len(nums2)/2]) / 2.0
        # if nums2 == []:
        #     if len(nums1) % 2 == 1:
        #         return float(nums1[len(nums1)/2])
        #     return (nums1[len(nums1)/2-1] + nums1[len(nums1)/2]) / 2.0
        # if nums1[0] >= nums2[-1]:
        #     nums = nums2 + nums1
        #     if len(nums) % 2 == 1:
        #         return float(nums[len(nums)/2])
        #     return (nums[len(nums)/2-1] + nums[len(nums)/2]) / 2.0
        # if nums2[0] >= nums1[-1]:
        #     nums = nums1 + nums2
        #     if len(nums) % 2 == 1:
        #         return float(nums[len(nums)/2])
        #     return (nums[len(nums)/2-1] + nums[len(nums)/2]) / 2.0
        # # always make nums1 have less elements for simplify
        # if len(nums1) > len(nums2):
        #     nums1, nums2 = nums2, nums1
        # l1 = len(nums1)
        # l2 = len(nums2)
        # l = l1 + l2
        # # first deal with special cases: l1 == 1
        # if l1 == 1:
        #     if l2 % 2 == 0:
        #         p2 = l2 / 2 - 1
        #         if nums1[0] <= nums2[p2]:
        #             return float(nums2[p2])
        #         if nums1[0] >= nums2[p2+1]:
        #             return float(nums2[p2+1])
        #         return float(nums1[0])
        #     else:
        #         if l2 == 1:
        #             return (nums1[0] + nums2[0]) / 2.0
        #         p2 = l2/2
        #         if nums1[0] <= nums2[p2 - 1]:
        #             return (nums2[p2-1] + nums2[p2]) / 2.0
        #         if nums1[0] <= nums2[p2 + 1]:
        #             return (nums1[0] + nums2[p2]) / 2.0
        #         return (nums2[p2+1] + nums2[p2]) / 2.0
        # # and special case: l1 = 2
        # if l1 == 2:
        #     if l2 % 2 == 0:
        #         if l2 == 2:
        #             if nums1[1] <= nums2[0]:
        #                 return (nums1[1] + nums2[0]) / 2.0
        #             elif nums1[0] >= nums2[1]:
        #                 return (nums1[0] + nums2[1]) / 2.0
        #             return (max(nums1[0], nums2[0]) + min(nums1[1], nums2[1])) / 2.0
        #         p2 = (l2-1) / 2
        #         if nums1[1] <= nums2[p2]:
        #             return (max(nums1[1], nums2[p2-1]) + nums2[p2]) / 2.0
        #         elif nums1[0] >= nums2[p2+1]:
        #             return (nums2[p2 + 1] + min(nums2[p2 + 2], nums1[0])) / 2.0
        #         return (max(nums1[0], nums2[p2]) + min(nums1[1], nums2[p2+1])) / 2.0
        #     else:
        #         p2 = (l2-1) / 2
        #         if nums1[1] <= nums2[p2]:
        #             return max(nums1[1], nums2[p2-1])
        #         if nums1[0] >= nums2[p2+1]:
        #             return nums2[p2+1]
        #         return sorted(nums1 + [nums2[p2], nums2[p2+1]])[1]
        #
        # # special cases: nums1 are all on lower side, or nums1 are all on higher side
        # if l % 2 == 1:
        #     p2 = l/2 - l1
        #     if nums1[-1] <= nums2[p2]:
        #         return nums2[p2]
        #     p2 = l/2
        #     if nums1[0] >= nums2[p2]:
        #         return nums2[p2]
        # elif l1 != l2:
        #     # here only check l1 < l2 case
        #     # for l1 == l2 in this special case, already considered in the beginning
        #     p2 = l/2 - 1 - l1
        #     if nums1[-1] <= nums2[p2]:
        #         return (nums2[p2] + nums2[p2 + 1]) / 2.0
        #     p2 = l/2
        #     if nums1[0] >= nums2[p2]:
        #         return (nums2[p2] + nums2[p2 - 1]) / 2.0
        # # now it is guaranteed nums1 and nums2 all fall in both lower side and higher side, and l2 >= l1 >= 3
        # if l1 % 2 == l2 % 2:
        #     p1 = 0
        #     p2 = l/2-2
        #     shift_low = 0
        #     shift_high = min(l1-1, p2+1)
        #     if nums1[p1] <= nums2[p2+1] and nums2[p2] <= nums1[p1+1]:
        #         # found it!
        #         return (max(nums1[p1], nums2[p2]) + min(nums1[p1+1], nums2[p2+1])) / 2.0
        #     while shift_high - shift_low > 1:
        #         shift_mid = (shift_high + shift_low) / 2
        #         p1t = p1 + shift_mid
        #         p2t = p2 - shift_mid
        #         # print nums1[p1t], nums1[p1t+1], nums2[p2t], nums2[p2t+1]
        #         if nums1[p1t] <= nums2[p2t+1] and nums2[p2t] <= nums1[p1t+1]:
        #             # found it!
        #             return (max(nums1[p1t], nums2[p2t]) + min(nums1[p1t+1], nums2[p2t+1])) / 2.0
        #         if nums2[p2t+1] < nums1[p1t]:
        #             # too large!
        #             shift_high = shift_mid
        #             continue
        #         if nums2[p2t] > nums1[p1t+1]:
        #             # too small!
        #             shift_low = shift_mid
        #     p1t = p1 + shift_low
        #     p2t = p2 - shift_low
        #     return (max(nums1[p1t], nums2[p2t]) + min(nums1[p1t+1], nums2[p2t+1])) / 2.0
        # p1 = 0
        # p2 = (l-5)/2
        # shift_low = 0
        # shift_high = min(l1-1, p2+1)
        # if nums1[p1] <= nums2[p2+1] and nums2[p2] <= nums1[p1+1]:
        #     # found it!
        #     return max(max(nums1[p1], nums2[p2]), min(nums1[p1+1], nums2[p2+1]))
        # while shift_high - shift_low > 1:
        #     shift_mid = (shift_high + shift_low) / 2
        #     p1t = p1 + shift_mid
        #     p2t = p2 - shift_mid
        #     # print nums1[p1t], nums1[p1t+1], nums2[p2t], nums2[p2t+1]
        #     if nums1[p1t] <= nums2[p2t+1] and nums2[p2t] <= nums1[p1t+1]:
        #         # found it!
        #         return max(max(nums1[p1t], nums2[p2t]), min(nums1[p1t+1], nums2[p2t+1]))
        #     if nums2[p2t+1] < nums1[p1t]:
        #         # too large!
        #         shift_high = shift_mid
        #         continue
        #     if nums2[p2t] > nums1[p1t+1]:
        #         # too small!
        #         shift_low = shift_mid
        # p1t = p1 + shift_low
        # p2t = p2 - shift_low
        # return max(max(nums1[p1t], nums2[p2t]), min(nums1[p1t+1], nums2[p2t+1]))

        # solution 3 bit simplified but using for not binary search
        # if nums1 == [] and nums2 == []:
        #     # error i think
        #     return 0
        # if nums1 == []:
        #     if len(nums2) % 2 == 1:
        #         return float(nums2[len(nums2)/2])
        #     return (nums2[len(nums2)/2-1] + nums2[len(nums2)/2]) / 2.0
        # if nums2 == []:
        #     if len(nums1) % 2 == 1:
        #         return float(nums1[len(nums1)/2])
        #     return (nums1[len(nums1)/2-1] + nums1[len(nums1)/2]) / 2.0
        # l1 = len(nums1)
        # l2 = len(nums2)
        # l = l1 + l2
        # if l1 > l2:
        #     nums1, nums2 = nums2, nums1
        #     l1, l2 = l2, l1
        # p1 = -1
        # p2 = l/2-1
        # shift_low = 0
        # shift_high = l1+1
        # def verify(p1, p2):
        #     # shift too small! p1 too small & p2 too large => ~p2-p1 > 0, return 1
        #     if p1 < 0 or p2 >= l2-1:
        #         if nums1[p1+1] >= nums2[p2]:
        #             return 0
        #         return 1
        #     # shift too large! p1 too large & p2 too small => ~p2-p1 < 0, return -1
        #     if p1 >= l1-1 or p2 < 0:
        #         if nums2[p2+1] >= nums1[p1]:
        #             return 0
        #         return -1
        #     # return (nums1[p1+1] >= nums2[p2] and nums2[p2+1] <= nums1[p1]) ==> 0
        #     if nums1[p1+1] < nums2[p2]:
        #         return 1
        #     if nums2[p2+1] < nums1[p1]:
        #         return -1
        #     return 0
        # for i in range(shift_low, shift_high):
        #     p1t = p1 + i
        #     p2t = p2 - i
        #     if verify(p1t, p2t) == 0:
        #         lower = []
        #         higher = []
        #         if p1t >= 0:
        #             lower.append(nums1[p1t])
        #         if p1t <= l1-2:
        #             higher.append(nums1[p1t+1])
        #         if p2t >= 0:
        #             lower.append(nums2[p2t])
        #         if p2t <= l2-2:
        #             higher.append(nums2[p2t+1])
        #         break
        # if l % 2 == 0:
        #     return (max(lower) + min(higher)) / 2.0
        # return float(min(higher))

        # solution 4 - hit 88.91%! still more space to improve?
        if nums1 == [] and nums2 == []:
            # error i think
            return 0
        if nums1 == []:
            if len(nums2) % 2 == 1:
                return float(nums2[len(nums2)/2])
            return (nums2[len(nums2)/2-1] + nums2[len(nums2)/2]) / 2.0
        if nums2 == []:
            if len(nums1) % 2 == 1:
                return float(nums1[len(nums1)/2])
            return (nums1[len(nums1)/2-1] + nums1[len(nums1)/2]) / 2.0
        l1 = len(nums1)
        l2 = len(nums2)
        l = l1 + l2
        if l1 > l2:
            nums1, nums2 = nums2, nums1
            l1, l2 = l2, l1
        p1 = -1
        p2 = l/2-1
        shift_low = 0
        shift_high = l1+1
        def verify(p1, p2):
            # shift too small! p1 too small & p2 too large => ~p2-p1 > 0, return 1
            if p1 < 0 or p2 >= l2-1:
                if nums1[p1+1] >= nums2[p2]:
                    return 0
                return 1
            # shift too large! p1 too large & p2 too small => ~p2-p1 < 0, return -1
            if p1 >= l1-1 or p2 < 0:
                if nums2[p2+1] >= nums1[p1]:
                    return 0
                return -1
            # return (nums1[p1+1] >= nums2[p2] and nums2[p2+1] <= nums1[p1]) ==> 0
            if nums1[p1+1] < nums2[p2]:
                return 1
            if nums2[p2+1] < nums1[p1]:
                return -1
            return 0

        lower = []
        higher = []
        while shift_high - shift_low > 1:
            shift_mid = (shift_high + shift_low) / 2
            p1t = p1 + shift_mid
            p2t = p2 - shift_mid
            r = verify(p1t, p2t)
            if r == 0:
                if p1t >= 0:
                    lower.append(nums1[p1t])
                if p1t <= l1-2:
                    higher.append(nums1[p1t+1])
                if p2t >= 0:
                    lower.append(nums2[p2t])
                if p2t <= l2-2:
                    higher.append(nums2[p2t+1])
                break
            if r > 0:
                shift_low = shift_mid
                continue
            if r < 0:
                shift_high = shift_mid
        if lower == []:
            p1t = p1 + shift_low
            p2t = p2 + shift_low
            if p1t >= 0:
                lower.append(nums1[p1t])
            if p1t <= l1-2:
                higher.append(nums1[p1t+1])
            if p2t >= 0:
                lower.append(nums2[p2t])
            if p2t <= l2-2:
                higher.append(nums2[p2t+1])
        if l % 2 == 0:
            return (max(lower) + min(higher)) / 2.0
        return float(min(higher))






s = Solution()
print s.findMedianSortedArrays([1, 4], [2, 3, 5, 6])
