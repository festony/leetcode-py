#!/usr/bin/env python2
# https://leetcode.com/problems/coin-change/description/
from datetime import datetime
start = datetime.now()

class Solution(object):
    def coinChange(self, coins, amount):
        """
        :type coins: List[int]
        :type amount: int
        :rtype: int
        """
        # solution 1 - DP(actually search?) - should work but just too slow for big input.
        # cache = {}
        # def calc_amount(a):
        #     if a in cache:
        #         return cache[a]
        #     if a in coins:
        #         cache[a] = 1
        #         return 1
        #     min_r = -1
        #     for split1 in range(1, a/2+1):
        #         split2 = a - split1
        #         r1 = calc_amount(split1)
        #         r2 = calc_amount(split2)
        #         if r1 > 0 and r2 > 0:
        #             r = r1 + r2
        #             if min_r < 0 or r < min_r:
        #                 min_r = r
        #     cache[a] = min_r
        #     return min_r
        # for i in range(1, amount):
        #     calc_amount(i)
        # return calc_amount(amount)

        # solution 2 - try greedy - still too slow
        # if amount == 0:
        #     return 0
        # coins.sort(reverse=True)
        # def try_fit(a, start_coin_index):
        #     if a < coins[start_coin_index]:
        #         if start_coin_index == len(coins) - 1:
        #             return -1
        #         return try_fit(a, start_coin_index + 1)
        #     if a == coins[start_coin_index]:
        #         return 1
        #     max_c = a / coins[start_coin_index]
        #     if a % coins[start_coin_index] == 0:
        #         return max_c
        #     if start_coin_index == len(coins) - 1:
        #         return -1
        #     min_r = a + 1
        #     for i in range(0, max_c + 1)[::-1]:
        #         rest = a - coins[start_coin_index] * i
        #         sub_r = try_fit(rest, start_coin_index + 1)
        #         if sub_r > 0 and i + sub_r < min_r:
        #             min_r = i + sub_r
        #     if min_r <= a:
        #         return min_r
        #     return -1
        # return try_fit(amount, 0)

        # solution 3 - branch and bound - 418ms/98.29%
        # if amount == 0:
        #     return 0
        # if not coins:
        #     return -1
        # coins.sort(reverse=True)
        # import math
        # self.min_r = int(math.ceil(float(amount) / coins[-1])) + 1
        # def try_comb(comb, a):
        #     start_index = len(comb)
        #     max_c = int(math.ceil(float(a) / coins[start_index]))
        #     best_guess = sum(comb) + max_c
        #     # worst_guess = sum(comb) + int(math.ceil(float(a) / coins[-1]))
        #     if best_guess > self.min_r:
        #         return -1
        #     if a % coins[start_index] == 0:
        #         r = sum(comb) + max_c
        #         if r < self.min_r:
        #             self.min_r = r
        #         return r
        #     if start_index == len(coins) - 1:
        #         return -1
        #     curr_min_r = a + 1
        #     for i in range(max_c)[::-1]:
        #         newcomb = comb + [i]
        #         newa = a - coins[start_index] * i
        #         curr_r = try_comb(newcomb, newa)
        #         if curr_r > 0 and curr_r < curr_min_r:
        #             curr_min_r = curr_r
        #             if curr_r < self.min_r:
        #                 self.min_r = curr_r
        #     if curr_min_r == a + 1:
        #         return -1
        #     return curr_min_r
        # try_comb([], amount)
        # if self.min_r == int(math.ceil(float(amount) / coins[-1])) + 1:
        #     return -1
        # return self.min_r

        # solution 4 - top to bottom DP(actually greedy?) - time exceeded
        if amount == 0:
            return 0
        if not coins:
            return -1
        min_coin = min(coins)
        status_coll = [[0, amount]]
        checked_amounts = set([])
        while status_coll:
            curr_status = status_coll.pop(0)
            if curr_status[1] in checked_amounts:
                continue
            if curr_status[1] in coins:
                return curr_status[0] + 1
            if curr_status[1] < min_coin:
                checked_amounts.add(curr_status[1])
                continue
            for c in coins:
                if c < curr_status[1]:
                    status_coll.append([curr_status[0] + 1, curr_status[1] - c])
                checked_amounts.add(curr_status[1])
        return -1


















s = Solution()

# print s.coinChange([1, 2, 5], 11)
# print s.coinChange([186, 419, 83, 408], 6249)
# print s.coinChange([284, 260, 393, 494], 7066)
print s.coinChange([86, 210, 29, 22, 402, 140, 16, 466], 3219)
# print s.coinChange([2], 1)

print datetime.now()-start
