#!/usr/bin/env python2
# https://leetcode.com/problems/longest-palindromic-substring/description/

class Solution(object):
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        # first write a plain slow bruteforce solution
        # but VERY slow!
        # substring of x: [start_i, end_i)
        # def check_is_pal(x, start_i, end_i):
        #     for o in range(0, (end_i - start_i) / 2):
        #         if x[start_i + o] != x[end_i - 1 - o]:
        #             return False
        #     return True
        #
        # longest_l = 1
        # longest = s[0]
        # if len(s) < 2:
        #     return s
        # for start_i in range(len(s) - 1):
        #     if len(s) - start_i < longest_l:
        #         return longest
        #     for end_i in range(start_i+2, len(s)+1)[::-1]:
        #         if end_i - start_i <= longest_l:
        #             break
        #         if check_is_pal(s, start_i, end_i):
        #             if (end_i - start_i) > longest_l:
        #                 longest = s[start_i:end_i]
        #                 longest_l = len(longest)
        #             break
        # return longest

        # second solution - use reverse string and longest 1 array checking, but think still too slow! 14.21%
        # rs = s[::-1]
        # def comp_str(s1, s2):
        #     r = [0] * len(s1)
        #     for i in range(len(s1)):
        #         if s1[i] == s2[i]:
        #             r[i] = 1
        #     # print s1
        #     # print s2
        #     return r
        #
        # def get_longest_all_1_length_in_middle(a):
        #     start_index = -1
        #     x = -1
        #
        #     for i in range(len(a)/2 + 1)[::-1]:
        #         if a[i] == 0 or a[len(a)-i-1] == 0:
        #             break
        #         x = i
        #     if x >= 0:
        #         return [x, len(a) - x]
        #     return []
        #
        # longest_l = 1
        # longest = s[0]
        # for i in range(0, len(s)):
        #     subs1 = s[i:]
        #     subrs1 = rs[:len(s)-i]
        #     l1 = get_longest_all_1_length_in_middle(comp_str(subs1, subrs1))
        #     if l1 != [] and l1[1] - l1[0] > longest_l:
        #         longest_l = l1[1] - l1[0]
        #         longest = subs1[l1[0]:l1[1]]
        #     subs2 = s[:len(s)-i]
        #     subrs2 = rs[i:]
        #     l2 = get_longest_all_1_length_in_middle(comp_str(subs2, subrs2))
        #     if l2 != [] and l2[1] - l2[0] > longest_l:
        #         longest_l = l2[1] - l2[0]
        #         longest = subs2[l2[0]:l2[1]]
        # return longest

        # solution 3 - still too slow! 22.78%
        # possible_p_centers = []
        # def get_expected_char_index(p_center, i):
        #     if p_center != int(p_center):
        #         # means expecting even pal, e.g. p_center = 2.5 for `cabba?`
        #         left_c = int(p_center)
        #         right_c = left_c + 1
        #         distance = i - right_c
        #         mirror_i = left_c - distance
        #         return mirror_i
        #     else:
        #         # means expecting odd pal, e.g. p_center = 3 for `cbadab?`
        #         distance = i - p_center
        #         mirrow_i = p_center - distance
        #         return mirrow_i
        #
        # def get_pal_range_from_p_center(p_center, i):
        #     if p_center != int(p_center):
        #         left_c = int(p_center)
        #         right_c = left_c + 1
        #         distance = i - right_c
        #         if distance < 0:
        #             return []
        #         if distance > left_c:
        #             distance = left_c
        #         mirror_i = left_c - distance
        #         return [mirror_i, right_c + distance + 1]
        #     else:
        #         distance = i - p_center
        #         if distance > p_center:
        #             distance = p_center
        #         mirrow_i = p_center - distance
        #         return [mirrow_i, i + 1]
        #
        # def check_p_center_hits_left_wall(p_center, i):
        #     if p_center != int(p_center):
        #         left_c = int(p_center)
        #         right_c = left_c + 1
        #         distance = i - right_c
        #         return distance >= left_c
        #     else:
        #         distance = i - p_center
        #         return distance >= p_center
        #
        # max_l = 0
        # max_pal = []
        #
        # for i in range(0, len(s)):
        #     # print 'possible_p_centers:', possible_p_centers
        #     p_to_remove = []
        #     for pp in possible_p_centers:
        #         c_i = get_expected_char_index(pp, i)
        #         if s[i] != s[c_i]:
        #             # print 'failed!'
        #             pal = get_pal_range_from_p_center(pp, i - 1)
        #             if pal != []:
        #                 l = pal[1] - pal[0]
        #                 if l > max_l:
        #                     max_l = l
        #                     max_pal = pal
        #             p_to_remove.append(pp)
        #         elif check_p_center_hits_left_wall(pp, i):
        #             # print 'hit left wall!'
        #             pal = get_pal_range_from_p_center(pp, i)
        #             l = pal[1] - pal[0]
        #             if pal != []:
        #                 l = pal[1] - pal[0]
        #                 if l > max_l:
        #                     max_l = l
        #                     max_pal = pal
        #             p_to_remove.append(pp)
        #     for pp in p_to_remove:
        #         possible_p_centers.remove(pp)
        #     if (len(s) - i) * 2 + 1 > max_l and i > 0:
        #         possible_p_centers.append(i)
        #     if (len(s) - i) * 2 > max_l:
        #         possible_p_centers.append(i + 0.5)
        # for pp in possible_p_centers:
        #     pal = get_pal_range_from_p_center(pp, len(s) - 1)
        #     if pal != []:
        #         l = pal[1] - pal[0]
        #         if l > max_l:
        #             max_l = l
        #             max_pal = pal
        # if max_l == 0:
        #     return s[0]
        # return s[max_pal[0]:max_pal[1]]

        # solution 4 - seems still O(n^2) but just give it a try (actually it went worse)
        # indexes_by_char = {}
        # # indexes_by_distance = {}
        # # distances_by_index = {}
        # center_by_distance = {}
        # for i, c in enumerate(s):
        #     if c not in indexes_by_char:
        #         indexes_by_char[c] = []
        #     for pi in indexes_by_char[c]:
        #         d = i - pi
        #         # if d not in indexes_by_distance:
        #         #     indexes_by_distance[d] = []
        #         # indexes_by_distance[d].append(pi)
        #         # if pi not in distances_by_index:
        #         #     distances_by_index[pi] = []
        #         # distances_by_index[pi].append(d)
        #         center = (i + pi) / 2.0
        #         if d not in center_by_distance:
        #             center_by_distance[d] = []
        #         center_by_distance[d].append(center)
        #     indexes_by_char[c].append(i)
        #
        # max_l = 0
        # max_l_pal = []
        # # even
        # max_d = 0
        # max_d_c = -1
        # start_d = 1
        # centers = []
        # while start_d <= len(s):
        #     if start_d not in center_by_distance or center_by_distance[start_d] == []:
        #         break
        #     if start_d == 1:
        #         centers = center_by_distance[start_d]
        #         max_d = start_d
        #         max_d_c = centers[0]
        #     else:
        #         centers = filter(lambda x: x in center_by_distance[start_d], centers)
        #         if centers == []:
        #             break
        #         max_d = start_d
        #         max_d_c = centers[0]
        #     start_d += 2
        # if max_d > 0:
        #     max_l = max_d + 1
        #     max_l_pal = [int(max_d_c) - max_d / 2, int(max_d_c) + max_d / 2 + 2]
        #
        # # odd
        # max_d = 0
        # max_d_c = -1
        # start_d = 2
        # centers = []
        # while start_d <= len(s):
        #     if start_d not in center_by_distance or center_by_distance[start_d] == []:
        #         break
        #     if start_d == 2:
        #         centers = center_by_distance[start_d]
        #         max_d = start_d
        #         max_d_c = centers[0]
        #     else:
        #         centers = filter(lambda x: x in center_by_distance[start_d], centers)
        #         if centers == []:
        #             break
        #         max_d = start_d
        #         max_d_c = centers[0]
        #     start_d += 2
        # if max_d > 0 and max_l < max_d + 1:
        #     max_l = max_d + 1
        #     max_l_pal = [int(max_d_c) - max_d / 2, int(max_d_c) + max_d / 2 + 1]
        #
        # if max_l == 0:
        #     return s[0]
        # return s[max_l_pal[0]:max_l_pal[1]]

        # solution 5 - theoretically can do it in ~O(n^2) with divide & conquer
        # so can divide one section into 2, either palindrom in separate sections or on the edge
        # but if start with blocks contains only 1 sort of char, then ensure when merging all (possible?) palindroms are on edges
        # hit 78.22% ... still not good enough :(

        # blocks = []
        # max_l = 0
        # max_p = ''
        #
        # # theory: 1. initially, split string into blocks, each init block only contains same char. e.g. abfdjjjjaaaas => a b f d jjjj aaaa s
        # # 2. then merge these blocks two by two => when merging, the left and right of the merging contact will never be the same char.
        # # as a result, we only need to consider the extending of palindrom on the side of each block.
        # # e.g. aabbaafdfdxccc => [[[a]a]bbaa]fdfdx[c[c[c]]], left side we consider a, or aa, or aabbaa; right side we consider c, cc, ccc
        # # there might be palindrom in the middle, but such middle palindroms must be processed already in previous merging.
        # # and, when merging 2 blocks: b1 -->merge<-- b2, obviously we only need to consider the right side of b1 and left side of b2.
        # #
        # # merge:
        # # in merging, we consider the expanding of palindroms:
        # # e.g. ......bcaaa >-< caxff
        # # consider right side of left block:  [c]aaa >-< [c]axff => from aaa to caaac, got a new palindrom!
        # # but then [b]caaa >-< c[a] => matching end, as this is in the middle of the merge result string, no possibility for it to continue expand
        # # thus now we can record caaac (if it's a longer palindrom than recorded one), and forget it, ignore it in our further consideration.
        # #
        # # consider second case:
        # # ....yxbc[aaa] >-< cbx
        # # obviously ....y[xbc]aaa >-< [cbx] => xbcaaacbx forms a palindrom; in this case the palindrom reaches the right edge of right block
        # # thus we need to put this one into our further consideration while continuing with matching when merge with next right block.
        # #
        # # so after each merge, we get less blocks, and for each block we only consider their two sides.
        # # divide and conquer, plus a little bit branch and bound.
        #
        # for i in range(len(s) + 1):
        #     if i == 0:
        #         block_start = 0
        #         continue
        #     if i == len(s) or s[i] != s[i-1]:
        #         blocks.append({'body': s[block_start:i], 'left': range(1, (i-block_start+1))[::-1], 'right': range(1, (i-block_start+1))[::-1]})
        #         if (i-block_start) > max_l:
        #             max_l = i-block_start
        #             max_p = s[block_start:i]
        #         block_start = i
        #
        # def merge_2_blocks(b1, b2):
        #     local_max_l = 0
        #     local_max_p = ''
        #     b = {'body': b1['body']+b2['body'], 'left': b1['left'], 'right': b2['right']}
        #
        #     # try extend pal body of b1 right
        #     rmv = []
        #     appd_rgt = []
        #     appd_lft = []
        #     for r in b1['right']:
        #         if r < len(b1['body']):
        #             ended = False
        #             o = 1
        #             while len(b1['body']) - r - o >= 0 and o - 1 <= len(b2['body']) - 1:
        #                 if b1['body'][len(b1['body']) - r - o] != b2['body'][o - 1]:
        #                     ended = True
        #                     break
        #                 o += 1
        #             o -= 1
        #             if ended:
        #                 rmv.append(r)
        #             if o > 0:
        #                 new_p = b1['body'][len(b1['body']) - r - o:] + b2['body'][:o]
        #                 new_l = len(new_p)
        #                 if not ended and o >= len(b2['body']) - 1:
        #                     appd_rgt.append(new_l)
        #                 if not ended and o + r >= len(b1['body']) - 1:
        #                     appd_lft.append(new_l)
        #                 if new_l > local_max_l:
        #                     local_max_l = new_l
        #                     local_max_p = new_p
        #     b1['right'] = appd_lft
        #     if 1 not in b1['right']:
        #         b1['right'].append(1)
        #     b1['right'].sort(reverse = True)
        #     for l in appd_lft:
        #         b1['left'].append(l)
        #     b1['left'].sort(reverse = True)
        #
        #     rmv = []
        #     appd_rgt = []
        #     appd_lft = []
        #     for l in b2['left']:
        #         if l < len(b2['body']):
        #             ended = False
        #             o = 1
        #             while len(b1['body']) - o >= 0 and l + o - 1 <= len(b2['body']) - 1:
        #                 if b1['body'][len(b1['body']) - o] != b2['body'][l + o - 1]:
        #                     ended = True
        #                     break
        #                 o += 1
        #             o -= 1
        #             if ended:
        #                 rmv.append(l)
        #             if o > 0:
        #                 new_p = b1['body'][len(b1['body']) - o:] + b2['body'][:l + o]
        #                 new_l = len(new_p)
        #                 if o == len(b1['body']):
        #                     appd_lft.append(new_l)
        #                 if o + l == len(b2['body']):
        #                     appd_rgt.append(new_l)
        #                 if new_l > local_max_l:
        #                     local_max_l = new_l
        #                     local_max_p = new_p
        #     b2['left'] = appd_lft
        #     if 1 not in b2['left']:
        #         b2['left'].append(1)
        #     b2['left'].sort(reverse = True)
        #     for r in appd_rgt:
        #         b2['right'].append(r)
        #     b2['right'].sort(reverse = True)
        #
        #     return b, local_max_l, local_max_p
        #
        # while len(blocks) > 1:
        #     new_blocks = []
        #     while len(blocks) > 1:
        #         b1 = blocks.pop(0)
        #         b2 = blocks.pop(0)
        #         b, l_max_l, l_max_p = merge_2_blocks(b1, b2)
        #         if l_max_l > max_l:
        #             max_l = l_max_l
        #             max_p = l_max_p
        #         new_blocks.append(b)
        #     for b in blocks:
        #         new_blocks.append(b)
        #     blocks = new_blocks
        # return max_p

        # solution 6 - try DP!
        # =_= but slower! 77.90%

        # # bbbaacaa|bbb
        # # 01234567 89A - txt index
        # # 01234567 012 - txt1/txt2 index
        # # 3210xxx0 123 - o
        #
        # def check_pal_right_side(text_left, right_side_pal_len_of_left, text_right):
        #     o = 1
        #     while o <= len(text_right) and len(text_left)-right_side_pal_len_of_left-o >= 0:
        #         if text_right[o-1] != text_left[len(text_left)-right_side_pal_len_of_left-o]:
        #             break
        #         o += 1
        #     o -= 1
        #     return o
        #
        # # xxx|yqzqyxxx
        # # 012 3456789A - txt index
        # # 012 01234567 - txt1/txt2 index
        # # 321 0xxx0123 - o
        #
        # def check_pal_left_side(text_left, left_side_pal_len_of_right, text_right):
        #     o = 1
        #     while o <=len(text_left) and left_side_pal_len_of_right + o <= len(text_right):
        #         if text_left[len(text_left) - o] != text_right[left_side_pal_len_of_right + o - 1]:
        #             break
        #         o += 1
        #     o -= 1
        #     return o
        #
        # def findPalindrome(txt):
        #     if txt == '':
        #         # shouldn't reach here
        #         return [0, 0], [], []
        #     if len(set(list(txt))) == 1:
        #         return [0, len(txt)], range(1, len(txt)+1), range(1, len(txt)+1)
        #     # print txt
        #     m = (len(txt)-1) / 2
        #     # print m
        #     while m > 0 and txt[m] == txt[m+1]:
        #         m -= 1
        #     # print m
        #     if txt[m] == txt[m+1]:
        #         m = (len(txt)-1) / 2
        #         while m < len(txt) - 2 and txt[m] == txt[m+1]:
        #             m += 1
        #         # print m
        #     txt1 = txt[:m+1]
        #     txt2 = txt[m+1:]
        #     # print txt1, txt2
        #     r1 = findPalindrome(txt1)
        #     r2 = findPalindrome(txt2)
        #     # print r1, r2
        #     new_left = r1[1]
        #     new_right = r2[2]
        #     if r1[0][1] - r1[0][0] > r2[0][1] - r2[0][0]:
        #         longest = r1[0]
        #     else:
        #         longest = [r2[0][0]+len(txt1), r2[0][1]+len(txt1)]
        #     # print longest
        #     # check r1 right side:
        #     # bbbaacaa|bbb
        #     # 01234567 89A
        #     # lth1 = 5
        #     # m + 1 = 8
        #     # m = 7
        #     for lth1 in r1[2]:
        #         o = check_pal_right_side(txt1, lth1, txt2)
        #         if (lth1 + o * 2) > longest[1] - longest[0]:
        #             longest = [m+1 - lth1 - o, m+1+o]
        #         if o == len(txt2):
        #             new_right.append(lth1+o*2)
        #         if o + lth1 == len(txt1):
        #             new_left.append(lth1+o*2)
        #     # check r2 left side:
        #     # xxx|yqzqyxxx
        #     # 012 3456789A
        #     # lth2 = 5
        #     # m + 1 = 3
        #     # m = 2
        #     for lth2 in r2[1]:
        #         o = check_pal_left_side(txt1, lth2, txt2)
        #         if (lth2 + o * 2) > longest[1] - longest[0]:
        #             longest = [m+1 - o, m+1 + lth2 + o]
        #         if o == len(txt1):
        #             new_left.append(lth2+o*2)
        #         if o + lth2 == len(txt2):
        #             new_right.append(lth2+o*2)
        #
        #     # print txt, longest, new_left, new_right
        #     return longest, list(set(new_left)), list(set(new_right))
        # r = findPalindrome(s)
        # return s[r[0][0]:r[0][1]]

        # solution 7 - Manacher's algorithm! Go try it
        # ====> not getting right result, give up this section, starting from scratch
        # pals = [0, 1] * len(s) + [0]
        # toBeVerified = []
        # c = 2
        # i = 1
        # while i < len(s):
        #     # matching = True
        #     indexToCompare = c - i - 1
        #     print pals, i, c, indexToCompare
        #     if indexToCompare < 0:
        #         # palindrom ends at prev char (i-1) - hit left edge
        #         # now to verify the copied palindrom length records from mirror node
        #         single_side_len = pals[c] - 1
        #         toBeRemoved = []
        #         for v in toBeVerified:
        #             allowed_l = single_side_len - (v - c)
        #             v_s_len = pals[v] - 1
        #             if v_s_len >= allowed_l:
        #                 pals[v] = allowed_l + 1
        #             else:
        #                 # can confirm, no need to check again later
        #                 toBeRemoved.append(v)
        #         print 'hit left', toBeVerified, toBeRemoved
        #         for r in toBeRemoved:
        #             toBeVerified.remove(r)
        #         print toBeVerified
        #         if toBeVerified != []:
        #             c = toBeVerified.pop(0)
        #             print 'popped', c, toBeVerified
        #         else:
        #             # if c < 2*i-1:
        #             #     c = 2*i-1
        #             # elif c < 2*i:
        #             #     c = 2*i
        #             # else:
        #             #     c = 2*i+1
        #             #     i += 1
        #             c = 2*i+1
        #             i += 1
        #         continue
        #     if s[indexToCompare] == s[i]:
        #         # still matching
        #         print 'matching:', indexToCompare, c, i, 2*i, 2*i+1
        #         pals[c] += 2
        #         pals[2*i] = pals[2*c-2*i]
        #         if pals[2*i] > 2 or indexToCompare == 0:
        #             toBeVerified.append(2*i)
        #         pals[2*i+1] = pals[2*c-2*i-1]
        #         if pals[2*i+1] > 1 or indexToCompare == 0:
        #             toBeVerified.append(2*i+1)
        #         i += 1
        #     else:
        #         # palindrom ends at prev char (i-1)
        #         # now to verify the copied palindrom length records from mirror node
        #         single_side_len = pals[c] - 1
        #         toBeRemoved = []
        #         for v in toBeVerified:
        #             allowed_l = single_side_len - (v - c)
        #             if v % 2 == 0:
        #                 v_s_len = pals[v] - 1
        #                 if v_s_len > allowed_l:
        #                     pals[v] = allowed_l + 1
        #                 else:
        #                     # can confirm, no need to check again later
        #                     toBeRemoved.append(v)
        #             else:
        #                 v_s_len = (pals[v] + 1) / 2
        #                 if v_s_len > allowed_l:
        #                     pals[v] = allowed_l + 1
        #                 else:
        #                     # can confirm, no need to check again later
        #                     toBeRemoved.append(v)
        #         for r in toBeRemoved:
        #             toBeVerified.remove(r)
        #         print toBeVerified
        #         if toBeVerified != []:
        #             c = toBeVerified.pop(0)
        #         else:
        #             if c < 2*i-1:
        #                 c = 2*i-1
        #             elif c < 2*i:
        #                 c = 2*i
        #             else:
        #                 c = 2*i+1
        #                 i += 1
        # print pals
        # max_l_c = 0
        # max_l = pals[0]
        # for c, l in enumerate(pals):
        #     if max_l < l:
        #         max_l_c = c
        #         max_l = l
        # center = (max_l_c - 1) / 2
        # single_l = max_l / 2
        # if max_l % 2 == 0:als = [0, 1] * len(s) + [0]
        # toBeVerified = []
        # c = 2
        # i = 1
        # while i < len(s):
        #     # matching = True
        #     indexToCompare = c - i - 1
        #     print pals, i, c, indexToCompare
        #     if indexToCompare < 0:
        #         # palindrom ends at prev char (i-1) - hit left edge
        #         # now to verify the copied palindrom length records from mirror node
        #         single_side_len = pals[c] - 1
        #         toBeRemoved = []
        #         for v in toBeVerified:
        #             allowed_l = single_side_len - (v - c)
        #             v_s_len = pals[v] - 1
        #             if v_s_len >= allowed_l:
        #                 pals[v] = allowed_l + 1
        #             else:
        #                 # can confirm, no need to check again later
        #                 toBeRemoved.append(v)
        #         print 'hit left', toBeVerified, toBeRemoved
        #         for r in toBeRemoved:
        #             toBeVerified.remove(r)
        #         print toBeVerified
        #         if toBeVerified != []:
        #             c = toBeVerified.pop(0)
        #             print 'popped', c, toBeVerified
        #         else:
        #             # if c < 2*i-1:
        #             #     c = 2*i-1
        #             # elif c < 2*i:
        #             #     c = 2*i
        #             # else:
        #             #     c = 2*i+1
        #             #     i += 1
        #             c = 2*i+1
        #             i += 1
        #         continue
        #     if s[indexToCompare] == s[i]:
        #         # still matching
        #         print 'matching:', indexToCompare, c, i, 2*i, 2*i+1
        #         pals[c] += 2
        #         pals[2*i] = pals[2*c-2*i]
        #         if pals[2*i] > 2 or indexToCompare == 0:
        #             toBeVerified.append(2*i)
        #         pals[2*i+1] = pals[2*c-2*i-1]
        #         if pals[2*i+1] > 1 or indexToCompare == 0:
        #             toBeVerified.append(2*i+1)
        #         i += 1
        #     else:
        #         # palindrom ends at prev char (i-1)
        #         # now to verify the copied palindrom length records from mirror node
        #         single_side_len = pals[c] - 1
        #         toBeRemoved = []
        #         for v in toBeVerified:
        #             allowed_l = single_side_len - (v - c)
        #             if v % 2 == 0:
        #                 v_s_len = pals[v] - 1
        #                 if v_s_len > allowed_l:
        #                     pals[v] = allowed_l + 1
        #                 else:
        #                     # can confirm, no need to check again later
        #                     toBeRemoved.append(v)
        #             else:
        #                 v_s_len = (pals[v] + 1) / 2
        #                 if v_s_len > allowed_l:
        #                     pals[v] = allowed_l + 1
        #                 else:
        #                     # can confirm, no need to check again later
        #                     toBeRemoved.append(v)
        #         for r in toBeRemoved:
        #             toBeVerified.remove(r)
        #         print toBeVerified
        #         if toBeVerified != []:
        #             c = toBeVerified.pop(0)
        #         else:
        #             if c < 2*i-1:
        #                 c = 2*i-1
        #             elif c < 2*i:
        #                 c = 2*i
        #             else:
        #                 c = 2*i+1
        #                 i += 1
        # print pals
        # max_l_c = 0
        # max_l = pals[0]
        # for c, l in enumerate(pals):
        #     if max_l < l:
        #         max_l_c = c
        #         max_l = l
        # center = (max_l_c - 1) / 2
        # single_l = max_l / 2
        # if max_l % 2 == 0:
        #     left = center - single_l + 1
        #     right = center + single_l + 1
        # else:
        #     left = center - single_l
        #     right = center + single_l + 1
        # return s[left:right]
        #     left = center - single_l + 1
        #     right = center + single_l + 1
        # else:
        #     left = center - single_l
        #     right = center + single_l + 1
        # return s[left:right]
        # <==== giving up the above part, starting from scratch.

        # solution 7 - Manacher's algorithm! 2nd attempt
        # works! but slow =_= maybe not true Manacher - 34.87%
        chars = list(s)
        pals = [0, 1] * len(s) + [0]
        i = 1
        c = 2
        next_c = -1
        needs_verify = []
        verify = []
        while i < len(s) and c < len(s) * 2 + 1:
            p_i = i * 2 + 1
            p_ri = 2 * c - p_i
            ri = (p_ri - 1) / 2
            print pals, i, c, p_i, p_ri, ri, needs_verify
            if p_ri < 1 or chars[i] != chars[ri]:
                # hit left wall or fail matching
                # pal ends at i-1

                if i * 2 - 1 not in needs_verify and pals[c] > 2:
                    # always verify the right most if pal len > 2
                    needs_verify.append(i * 2 - 1)
                right_end = i-1
                p_right_end = right_end * 2 + 1
                verify = []
                for v in needs_verify:
                    right_reach = v + pals[v] - 1
                    if right_reach >= p_right_end:
                        verify.append(v)
                print verify
                if verify != []:
                    next_c = verify.pop(0)
                    pals[next_c] = p_right_end - next_c + 1
                    for v in verify:
                        pals[v] = pals[2 * next_c - v]
                needs_verify = verify

                if next_c > c:
                    c = next_c
                else:
                    if c < p_i - 2:
                        c = p_i - 2
                    elif c < p_i - 1:
                        c = p_i - 1
                    else:
                        c = p_i
                        i += 1
                print 'next:', c
                continue
            else:
                # chars[i] == chars[ri]
                # pal continues

                pals[c] += 2
                if p_i - 1 > c:
                    pals[p_i-1] = pals[p_ri+1]
                    if pals[p_i-1] > 0:
                        needs_verify.append(p_i-1)
                if p_i > c:
                    pals[p_i] = pals[p_ri]
                    if pals[p_i] >= 1:
                        needs_verify.append(p_i)

                # right_reach = p_i - 1 + pals[p_i - 1] - 1
                # right_reach = p_i + pals[p_i] - 1

                i += 1
                continue
            # end while
        print pals
        max_l_c = 0
        max_l = pals[0]
        for c, l in enumerate(pals):
            if max_l < l:
                max_l_c = c
                max_l = l
        center = (max_l_c - 1) / 2
        single_l = max_l / 2
        if max_l % 2 == 0:
            left = center - single_l + 1
            right = center + single_l + 1
        else:
            left = center - single_l
            right = center + single_l + 1
        return s[left:right]














# print s.longestPalindrome('babad')








# 1232145
# abaa
# bbaaa(b)
# (b)aaabb
# cbabAbab()
# (b)ab

s = Solution()



print s.longestPalindrome('bbbb')
# exit(0)


test_cases = []
test_cases.append([['babad'], 'bab'])
test_cases.append([['bb'], 'bb'])
test_cases.append([['bbb'], 'bbb'])
test_cases.append([['bbbb'], 'bbbb'])
test_cases.append([['bbbbb'], 'bbbbb'])
test_cases.append([['ababababa'], 'ababababa'])
test_cases.append([['fdskfjdslkajfksdajfl'], 'f'])
test_cases.append([['dddfjjjsffhs'], 'ddd'])
test_cases.append([['hs'], 'h'])
test_cases.append([['xfds9ruewivjn i8e'], 'x'])
test_cases.append([['2fjdkjsfdjkjjkjsjksfksjkdsfjk'], 'jkjjkj'])
test_cases.append([['2fjdkjsfdjkjjkasdfghgfdsaasdfghgfdsajsjksfksjkdsfjk'], 'asdfghgfdsaasdfghgfdsa'])
test_cases.append([['jkjjkj'], 'jkjjkj'])
test_cases.append([['adam'], 'ada'])
test_cases.append([['bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb'], 'bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb'])
test_cases.append([['kasdfghgfdsaasdfghgfdsaj'], 'asdfghgfdsaasdfghgfdsa'])
test_cases.append([['babadada'], 'adada'])
test_cases.append([['bananas'], 'anana'])

for tc in test_cases:
    expected = tc[1]
    actual = s.longestPalindrome(*tc[0])
    if expected != actual:
        print 'ERROR! for input {} expected {} but got {}'.format(tc[0], expected, actual)



# s.longestPalindrome('babad')

# print s.longestPalindrome('bb')
# print s.longestPalindrome('bbb')
# print s.longestPalindrome('bbbb')# print s.longestPalindrome('babad')
#
# print s.longestPalindrome('babad')
# print s.longestPalindrome('ababababa')
# print s.longestPalindrome('fdskfjdslkajfksdajfl')
# print s.longestPalindrome('dddfjjjsffhs')
# print s.longestPalindrome('hs')
# print s.longestPalindrome('xfds9ruewivjn i8e')
# print s.longestPalindrome('2fjdkjsfdjkjjkjsjksfksjkdsfjk')
# print s.longestPalindrome('2fjdkjsfdjkjjkasdfghgfdsaasdfghgfdsajsjksfksjkdsfjk')
# print s.longestPalindrome('abcdasdfghjkldcba')
# print s.longestPalindrome('jkjjkj')
# print s.longestPalindrome('bb')
# print s.longestPalindrome('bbbb')
# print s.longestPalindrome('adam')
# print s.longestPalindrome('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb')
# print s.longestPalindrome('kasdfghgfdsaasdfghgfdsaj')
# print s.longestPalindrome('babadada')
# print s.longestPalindrome('bananas')
# print s.longestPalindrome('dddfjjjsffhs')
# print s.longestPalindrome('ddd')
