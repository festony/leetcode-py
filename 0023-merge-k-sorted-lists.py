#!/usr/bin/env python2
# https://leetcode.com/problems/merge-k-sorted-lists/description/
from datetime import datetime
start = datetime.now()

# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def mergeKLists(self, lists):
        """
        :type lists: List[ListNode]
        :rtype: ListNode
        """
        # solution 1 - heap sort the first elem of each lists, put smallest into new list, O(log(k)*k*n) - 218ms/28.25%
        # # min heap
        # def adj_heap(heap, i, L):
        #     # adjust i-th elem in heap (a list) with it's child nodes. The first L elems in the list considered a heap
        #     # return 0: no swap; 1: swapped with right child; -1: swapped with left child
        #     lc_i = 2*i + 1
        #     rc_i = 2*i + 2
        #     if lc_i >= L:
        #         # no child node in heap
        #         return 0
        #     if rc_i >= L or heap[rc_i].val > heap[lc_i].val:
        #         # only left child node in heap
        #         if heap[i].val > heap[lc_i].val:
        #             heap[i], heap[lc_i] = heap[lc_i], heap[i]
        #             return -1
        #         return 0
        #     # now surely left child and right child are both existing
        #     if heap[i].val > heap[rc_i].val:
        #         heap[i], heap[rc_i] = heap[rc_i], heap[i]
        #         return 1
        #     return 0
        #
        # def prep_heap(heap, L):
        #     nodes = range(L)
        #     for i in nodes:
        #         r = adj_heap(heap, i, L)
        #         if r == 1:
        #             nodes.append(2*i+2)
        #         if r == -1:
        #             nodes.append(2*i+1)
        #     # while True:
        #     #     adjusted = False
        #     #     for i in range(L)[::-1]:
        #     #         r = adj_heap(heap, i, L)
        #     #         if r != 0:
        #     #             adjusted = True
        #     #     if not adjusted:
        #     #         return
        #
        # def max_heapify(heap, i, L):
        #     r = adj_heap(heap, i, L)
        #     if r == 1:
        #         max_heapify(heap, 2*i+2, L)
        #     if r == -1:
        #         max_heapify(heap, 2*i+1, L)
        #
        # pts = filter(lambda x:x is not None, lists)
        # if pts == []:
        #     return None
        # if len(pts) == 1:
        #     return pts[0]
        #
        # prep_heap(pts, len(pts))
        # head = pts[0]
        # p = head
        # while True:
        #     pts[0] = pts[0].next
        #     if pts[0] == None:
        #         if len(pts) == 1:
        #             break
        #         pts[0] = pts[-1]
        #         pts.pop()
        #     max_heapify(pts, 0, len(pts))
        #     p.next = pts[0]
        #     p = p.next
        # return head

        # solution 2 - merge lists 2 by 2 until only 1 list left, O(log(k)*k*n) - 158ms/49.32%
        if lists == []:
            return None
        if len(lists) == 1:
            return lists[0]
        lts = filter(lambda x:x is not None, lists)
        def merge_lists(l1, l2):
            p1 = l1
            p2 = l2
            if p1.val > p2.val:
                p = p2
                p2 = p2.next
            else:
                p = p1
                p1 = p1.next
            head = p
            while p1 is not None and p2 is not None:
                if p1.val > p2.val:
                    p.next = p2
                    p = p2
                    p2 = p2.next
                else:
                    p.next = p1
                    p = p1
                    p1 = p1.next
            if p1 is not None:
                p.next = p1
            if p2 is not None:
                p.next = p2
            return head
        while len(lts) > 1:
            l1 = lts.popleft()
            l2 = lts.popleft()
            lts.append(merge_lists(l1, l2))
        return lts[0]





s = Solution()

# print s.mergeKLists()

print datetime.now()-start
