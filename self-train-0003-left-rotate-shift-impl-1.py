# from programming pearls chap 2 - left rotate shift a string of length n by i
# e.g. for length n=8 string abcdefgh, left rotate shift for i=3 chars, we'll get: defghabc
# this is the first implementation: for i < n-i, swap first i chars and last i chars, the repeat for the remain part ([0:n-i])
# e.g.:
# [abc]defgh -> fgh|de[abc] => then do it for fghde: [fgh]de -is actually-> fgh[de] -> [de]h|fg
# => then do it for hfg: [h]fg -> g|f[h] => then do it for [g]f -> fg
# ===combine==>  defghabc

class Solution(object):
    def LeftRotateShift(self, s, i):
        """
        :type s: str
        :type i: int
        Left rotate shift string s for i chars.
        :rtype: str
        """
        r = list(s[:])
        n = len(r)

        if i == n - i:
            # just swap
            for x in range(i):
                r[x], r[n-i+x] = r[n-i+x], r[x]
            return ''.join(r)
        if i < n - i:
            for x in range(i):
                r[x], r[n-i+x] = r[n-i+x], r[x]
            return self.LeftRotateShift(r[:n-i], i) + ''.join(r[n-i:])
        # i > n - i
        i2 = n - i
        for x in range(i2):
            r[x], r[i+x] = r[i+x], r[x]
        return ''.join(r[:i2]) + self.LeftRotateShift(r[i2:], i - i2)

s = Solution()
print s.LeftRotateShift('abcdefgh', 1)
print s.LeftRotateShift('abcdefgh', 2)
print s.LeftRotateShift('abcdefgh', 3)
print s.LeftRotateShift('abcdefgh', 4)
print s.LeftRotateShift('abcdefgh', 5)
print s.LeftRotateShift('abcdefgh', 6)
print s.LeftRotateShift('abcdefgh', 7)
