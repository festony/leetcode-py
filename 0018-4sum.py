#!/usr/bin/env python2
# https://leetcode.com/problems/4sum/description/
from datetime import datetime
start = datetime.now()


class Solution(object):
    def fourSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[List[int]]
        """
        # solution 1 - recursive - too slow! O(n^4)
        # nums.sort()
        #
        # cache_lookFor = {}
        # def lookFor(low, up, t):
        #     k = (low, up, t)
        #     if k in cache_lookFor:
        #         return cache_lookFor[k]
        #     up_i = up
        #     low_i = low
        #     if t < nums[low_i] or t > nums[up_i - 1]:
        #         cache_lookFor[k] = -1
        #         return -1
        #     if t == nums[low_i]:
        #         cache_lookFor[k] = low_i
        #         return low_i
        #     if t == nums[up_i - 1]:
        #         cache_lookFor[k] = up_i - 1
        #         return up_i - 1
        #     while up_i > low_i + 1:
        #         mid_i = (up_i + low_i) / 2
        #         if nums[mid_i] == t:
        #             cache_lookFor[k] = mid_i
        #             return mid_i
        #         if nums[mid_i] > t:
        #             up_i = mid_i
        #         else:
        #             low_i = mid_i
        #     if nums[low_i] == t:
        #         cache_lookFor[k] = low_i
        #         return low_i
        #     cache_lookFor[k] = -1
        #     return -1
        #
        # cache_sum_2 = {}
        # def sum_2(low, up, t):
        #     k = (low, up, t)
        #     if k in cache_sum_2:
        #         return cache_sum_2[k]
        #     res = set([])
        #     for first_i in range(low, up-1):
        #         second_t = t - nums[first_i]
        #         second_i = lookFor(first_i + 1, up, second_t)
        #         if second_i >= 0:
        #             res.add((nums[first_i], nums[second_i]))
        #     cache_sum_2[k] = res
        #     return res
        #
        # cache_sum_3 = {}
        # def sum_3(low, up, t):
        #     k = (low, up, t)
        #     if k in cache_sum_3:
        #         return cache_sum_3[k]
        #     res = set([])
        #     for first_i in range(low, up-2):
        #         rest_t = t - nums[first_i]
        #         sub_res = sum_2(first_i + 1, up, rest_t)
        #         if len(sub_res) > 0:
        #             for r in sub_res:
        #                 res.add((nums[first_i], r[0], r[1]))
        #     cache_sum_3[k] = res
        #     return res
        #
        # def sum_4(low, up, t):
        #     res = set([])
        #     for first_i in range(low, up-3):
        #         rest_t = t - nums[first_i]
        #         sub_res = sum_3(first_i + 1, up, rest_t)
        #         if len(sub_res) > 0:
        #             for r in sub_res:
        #                 res.add((nums[first_i], r[0], r[1], r[2]))
        #     return res
        # return map(list, list(sum_4(0, len(nums), target)))

        # solution 2 - I think it's O(n^2) - 65.35%
        sum2 = {}
        for i in range(len(nums)-1):
            for j in range(i+1, len(nums)):
                s = nums[i] + nums[j]
                if s in sum2:
                    sum2[s].append([i, j])
                else:
                    sum2[s] = [[i, j]]
        keys = sum2.keys()
        keys.sort()
        left_p = 0
        right_p = len(keys) - 1
        res = set([])
        while left_p <= right_p:
            s = keys[left_p] + keys[right_p]
            if s == target:
                # do something
                left_nodes = sum2[keys[left_p]]
                right_nodes = sum2[keys[right_p]]
                for ln in left_nodes:
                    for rn in right_nodes:
                        if filter(lambda x:x in rn, ln) == []:
                            res.add(tuple(sorted([nums[ln[0]], nums[ln[1]], nums[rn[0]], nums[rn[1]]])))
                left_p += 1
                right_p -= 1
            elif s < target:
                left_p += 1
            else:
                right_p -= 1
        return map(list, list(res))











s = Solution()

print s.fourSum([1, 0, -1, 0, -2, 2], 0)

print datetime.now()-start
