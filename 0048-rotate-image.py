#!/usr/bin/env python2
# https://leetcode.com/problems/rotate-image/description/
from datetime import datetime
start=datetime.now()

class Solution(object):
    def rotate(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """
        # solution 1 - not meeting the requirement (use original matrix)
        # N = len(matrix)
        # nM = []
        # for h in range(0, N):
        #     line = [0]*N
        #     for w in range(0, N):
        #         line[w] = matrix[N-1-w][h]
        #     nM.append(line)
        # return nM

        # solution 2 - first flip by diagonal, then flip by middle vertical line
        # N = len(matrix)
        # for h in range(1, N):
        #     for w in range(0, h):
        #         temp = matrix[h][w]
        #         matrix[h][w] = matrix[w][h]
        #         matrix[w][h] = temp
        # for h in range(N):
        #     for w in range(N/2):
        #         temp = matrix[h][w]
        #         matrix[h][w] = matrix[h][N-1-w]
        #         matrix[h][N-1-w] = temp

        # solution 3 - do rotating one by one
        N = len(matrix)
        def rotate_one(h, w):
            # wrong: opposite way!
            # temp = matrix[h][w]
            # matrix[h][w] = matrix[w][N-1-h]
            # matrix[w][N-1-h] = matrix[N-1-h][N-1-w]
            # matrix[N-1-h][N-1-w] = matrix[N-1-w][h]
            # matrix[N-1-w][h] = temp
            temp = matrix[h][w]
            matrix[h][w] = matrix[N-1-w][h]
            matrix[N-1-w][h] = matrix[N-1-h][N-1-w]
            matrix[N-1-h][N-1-w] = matrix[w][N-1-h]
            matrix[w][N-1-h] = temp

        for h in range(N/2):
            for w in range(h, N-h-1):
                rotate_one(h, w)

# todo: use python's zip

s = Solution()
print s.rotate([
    [ 5, 1, 9,11],
    [ 2, 4, 8,10],
    [13, 3, 6, 7],
    [15,14,12,16]
])

print datetime.now()-start
