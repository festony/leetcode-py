#!/usr/bin/env python2
# https://en.wikipedia.org/wiki/Boyer%E2%80%93Moore_string_search_algorithm

class Solution(object):
    def BM(self, s, w):
        """
        :type s: str
        :type w: str
        Search for target string w in s.
        :rtype: str
        """
        # generate good suffix offsets
        rw = w[::-1]
        good_offsets = range(len(rw) + 1)
        o = 1
        i = 0
        while i+o < len(rw):
            if rw[i] == rw[i+o]:
                good_offsets[i+o+1] = o
                i += 1
            else:
                if i == 0:
                    o += 1
                else:
                    o += good_offsets[i]
                    i -= good_offsets[i]
        # print good_offsets

        # bad char cache just generate during searching
        bad_cache = {}
        # i is current mismatch char's index in pattern
        def get_bad_char_offset(i, c):
            if not c in bad_cache:
                bad_cache[c] = {}
            elif i in bad_cache[c]:
                return bad_cache[c][i]
            if i == len(rw) - 1:
                bad_cache[c][i] = 1
                return 1
            for x in range(i + 1, len(rw)):
                if rw[x] == c:
                    bad_cache[c][i] = x - i
                    return x - i
            bad_cache[c][i] = len(rw) - i
            return len(rw) - i

        i = 0
        m = len(rw) - 1
        while m < len(s):
            if rw[i] == s[m]:
                if i == len(rw) - 1:
                    # match!
                    print 'match! at', m
                    m += len(rw) - 1 + good_offsets[len(rw)]
                    i = 0
                else:
                    m -= 1
                    i += 1
            else:
                o = max(get_bad_char_offset(i, s[m]), good_offsets[i])
                m += i + o
                i = 0
        print 'matching completed!'




#
#   ABAB
# ABABAABC

s = Solution()

s.BM('fjdskfjfdjafkfjddjalfjdskfjdalaf', 'fjdskfjda')
s.BM('ABC ABCDAB ABCDABCDABDE', 'ABCDABD')
s.BM('ABC ABCDAB ABCDABCDABDE', 'AB')
s.BM('ABC ABCDAB ABCDABCDABDE', 'ABC')
s.BM('ABC ABCDAB ABCDABCDABDE', 'AAB')
s.BM('ABC ABCDAB ABCDABCDABDE', 'AAAA')
s.BM('ABC ABCDAB ABCDABCDABDE', 'ABAB')
s.BM('ABC ABCDAB ABCDABCDABDE', 'ABABC')
s.BM('ABC ABCDAB ABCDABCDABDE', 'ABCAB')

s.BM('barfoothefoobarman', 'foo')
