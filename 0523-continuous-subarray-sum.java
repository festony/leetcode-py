
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by festony on 8/03/18.
 */
public class Solution {
    // solution 2 - accepted but 100ms / 3.22%
    public boolean checkSubarraySum(int[] nums, int k) {

        // sum == n * k that n is also an integer, so 0 works
        for (int i=0; i<nums.length - 1; i++) {
            if (nums[i] == 0 && nums[i+1] == 0) {
                return true;
            }
        }

        if (k == 0) {
            // all nums are non-negative, thus if k == 0 then only continuous 0 sub seq can satisfy, which is already
            // checked above.
            return false;
        }

        if (nums.length < 2) {
            return false;
        }

        if (nums.length == 2) {
            return (nums[0] + nums[1]) % k == 0;
        }

        int nSum = Arrays.stream(nums).sum();
        if (nSum % k == 0) {
            // then the whole array already satisfies the condition.
            return true;
        }

        Map<Integer, Integer> headAccum = new HashMap<>();
        Map<Integer, Integer> tailAccum = new HashMap<>();
        int hSum = 0;
        int tSum = nSum;
        int tempSum;
        for (int i=0; i<nums.length; i++) {
            if (i > 0) {
                hSum += nums[i-1];
                tSum -= nums[i-1];
            }
            tempSum = hSum % k;
            if (!headAccum.containsKey(tempSum)) {
                headAccum.put(tempSum, i);
            }
            tempSum = tSum % k;
            tailAccum.put(tempSum, i);
        }
        tailAccum.put(0, nums.length);

        nSum %= k;

        for (int head : headAccum.keySet()) {
            int revHead = ((nSum + k) - head) % k;
            if (tailAccum.containsKey(revHead) && tailAccum.get(revHead) - headAccum.get(head) >= 2) {
                return true;
            }
        }
        return false;
    }
}
