#!/usr/bin/env python2
# https://leetcode.com/problems/decode-ways/description/
from datetime import datetime
start = datetime.now()

class Solution(object):
    def numDecodings(self, s):
        """
        :type s: str
        :rtype: int
        """
        # solution 1 - 53ms / 14.96%
        # if s == '':
        #     return 0
        # cache = {}
        # def calcDec(s):
        #     if s in cache:
        #         return cache[s]
        #     if s[0] == '0':
        #         cache[s] = 0
        #         return 0
        #     if len(s) == 1:
        #         cache[s] = 1
        #         return 1
        #     r = calcDec(s[1:])
        #     if (s[0] == '1') or (s[0] == '2' and s[1] in list('0123456')):
        #         if len(s) > 2:
        #             r += calcDec(s[2:])
        #         else:
        #             r += 1
        #     cache[s] = r
        #     return r
        # return calcDec(s)

        # solution 2 - try improve with divide & conquer
        # better - 46ms / 30.05%
        if s == '':
            return 0
        cache = {}
        def calcDec(s):
            if s in cache:
                return cache[s]
            if s[0] == '0':
                cache[s] = 0
                return 0
            if len(s) == 1:
                cache[s] = 1
                return 1
            if len(s) == 2:
                r = calcDec(s[0]) * calcDec(s[1])
                if s[0] == '1' or (s[0] == '2' and s[1] in set(list('0123456'))):
                    r += 1
                cache[s] = r
                return r
            post_mid_index = (len(s) - 1) / 2
            pre_mid_index = post_mid_index - 1
            r = calcDec(s[:post_mid_index]) * calcDec(s[post_mid_index:])
            if s[pre_mid_index] == '1' or (s[pre_mid_index] == '2' and s[post_mid_index] in set(list('0123456'))):
                r1 = calcDec(s[post_mid_index + 1:])
                if pre_mid_index > 0:
                    r1 *= calcDec(s[:pre_mid_index])
                r += r1
            cache[s] = r
            return r
        return calcDec(s)















s = Solution()

print s.numDecodings("1231323343104310")
# print s.numDecodings("12137")

print datetime.now()-start
