#!/usr/bin/env python2
# https://leetcode.com/problems/continuous-subarray-sum/description/
from datetime import datetime
start = datetime.now()

class Solution(object):
    def checkSubarraySum(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: bool
        """

        # solution 1 - very straightforward but O(n^2)
        # for i in range(len(nums) - 1):
        #     for j in range(i+2, len(nums)):
        #         if sum(nums[i:j]) % k == 0:
        #             return True
        # return False

        # edge case - k <= 0????? !!!!!

        # solution 2 - why still time exceeds? theoretically the complexity is O(n)
        # if k == 0:
        #     for i in range(len(nums) - 1):
        #         if nums[i] == 0 and nums[i+1] == 0:
        #             return True
        #     return False
        #
        # if k < 0:
        #     k = -k
        #
        # if len(nums) == 2:
        #     return sum(nums) % k == 0
        #
        # head_accu = {}
        # hsum = 0
        # head_accu[0] = 0
        # for i, n in enumerate(nums[:-2]):
        #     hsum += n
        #     msum = hsum % k
        #     if msum not in head_accu.keys():
        #         head_accu[msum] = i + 1
        # tail_accu = {}
        # tsum = 0
        # tail_accu[0] = len(nums)
        # for i, n in enumerate((nums[2:])[::-1]):
        #     tsum += n
        #     msum = tsum % k
        #     if msum not in tail_accu.keys():
        #         tail_accu[msum] = len(nums) - i - 1
        #
        # ssum = sum(nums) % k
        #
        # if ssum == 0 and 0 in head_accu.keys() and 0 in tail_accu.keys() and tail_accu[0] > head_accu[0] + 1:
        #     return True
        # for hk in head_accu.keys():
        #     rev_hk = ((k + ssum) - hk) % k
        #     if rev_hk in tail_accu.keys() and tail_accu[rev_hk] > head_accu[hk] + 1:
        #         return True
        # return False

        # solution 3 - now think i went into a bit over complicated way in solution 2 so fix it here
        # better! 57ms / 67.11%
        # so still not good enough!
        for i in range(len(nums) - 1):
            if nums[i] == 0 and nums[i+1] == 0:
                return True
        if k == 0:
            return False

        if k < 0:
            k = -k

        if len(nums) < 2:
            return False
        if len(nums) == 2:
            return (nums[0] + nums[1]) % k == 0

        accu = {}
        accu[0] = -1
        s = 0
        for i, n in enumerate(nums):
            s = (s + n) % k
            if s in accu:
                if i - accu[s] >= 2:
                    return True
            else:
                accu[s] = i
        return False




s = Solution()

print s.checkSubarraySum([2, 4, 6], 6)
print s.checkSubarraySum([3, 4, 6], 7)
print s.checkSubarraySum([3, 4, 6], 13)
print s.checkSubarraySum([1, 3, 4, 6], 17)
print s.checkSubarraySum([1, 3, 4, 6], 8)

print datetime.now()-start
