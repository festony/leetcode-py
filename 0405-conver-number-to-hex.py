#!/usr/bin/env python2
# https://leetcode.com/problems/convert-a-number-to-hexadecimal/description/
from datetime import datetime
start = datetime.now()

class Solution(object):
    def toHex(self, num):
        """
        :type num: int
        :rtype: str
        """
        neg = num < 0
        if neg:
            if num == -2147483648:
                return '80000000'
            num *= -1

        table = {}
        for i in range(10):
            table[i] = str(i)
        for i in range(10, 16):
            table[i] = chr(ord('a') + i - 10)
        # print table
        hex = []
        for i in range(8):
            cv = num & 15
            num >>= 4
            hex.append(cv)
        if neg:
            c = 1
            for i in range(8):
                hex[i] ^= 15
                hex[i] += c
                if hex[i] == 16:
                    c = 1
                    hex[i] = 0
                else:
                    c = 0
        head = True
        nhex = []
        for h in hex[::-1]:
            if h == 0 and head:
                continue
            head = False
            nhex.append(h)
        if nhex == []:
            nhex.append(0)


        # print hex
        return ''.join(map(lambda x:table[x], nhex))




















s = Solution()

print s.toHex(33)
print s.toHex(-33)
print s.toHex(-1)

print datetime.now()-start
