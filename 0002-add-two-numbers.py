#!/usr/bin/env python2
# https://leetcode.com/problems/add-two-numbers/description/

# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """

        # solution 1: only bit 73.28% .. gonna try something bad but fast
        # def addNode(n1, n2, c):
        #     v = c
        #     if n1 != None:
        #         v += n1.val
        #     if n2 != None:
        #         v += n2.val
        #     c  = v / 10
        #     v %= 10
        #     r = ListNode(v)
        #     return r, c
        # r, c = addNode(l1, l2, 0)
        # p1 = l1.next
        # p2 = l2.next
        # pr = r
        # while p1 != None or p2 != None:
        #     n, c = addNode(p1, p2, c)
        #     pr.next = n
        #     pr = n
        #     if p1 != None:
        #         p1 = p1.next
        #     if p2 != None:
        #         p2 = p2.next
        # if c > 0:
        #     pr.next = ListNode(c)
        # return r

        # solution 2: bad but bit faster - hit 99.79%
        def addNode(n1, v2, c):
            v = c + n1.val + v2
            c  = v / 10
            v %= 10
            n1.val = v
            return c
        c = addNode(l1, l2.val, 0)
        p1p = l1
        p2p = l2
        p1 = l1.next
        p2 = l2.next

        while p1 != None and p2 != None:
            c = addNode(p1, p2.val, c)
            p1p = p1
            p2p = p2
            p1 = p1.next
            p2 = p2.next
        if p2 != None:
            p1p.next = p2
            p1 = p2
        while c > 0:
            if p1 == None:
                p1p.next = ListNode(c)
                break
            c = addNode(p1, 0, c)
            p1p = p1
            p1 = p1.next
        return l1
