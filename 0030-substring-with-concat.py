#!/usr/bin/env python2
# https://leetcode.com/problems/substring-with-concatenation-of-all-words/description/
from datetime import datetime
start = datetime.now()


class Solution(object):
    def findSubstring(self, s, words):
        """
        :type s: str
        :type words: List[str]
        :rtype: List[int]
        """
        # solution 1 - thinking: separately find all the indexes of single words, then try combine the indexes to find continuous sequence that fulfill the criterion.
        # but too troublesome! Suddenly got a new thought

        # solution 2 - let L be the length of word, use offsets 0 ~ L-1 to split s into length L sections, check each section sequence for continuous sub sequence that fulfill the criterion.
        # complexity - best organized: ~ O(L*(len_s/L)*log(len_words)) = O(len_s*log(len_words)) (not sure if possible)
        # or worst case - O(len_s * len_words * log(len_words))
        # actually if count in the string comparison time which I expect to be ~O(len(words[0])), it gets worse to O(len_s * len_words * log(len_words) * len_words_0)
        # - 165ms/58.49%
        if not words:
            return []
        res = []
        s_words = sorted(words)
        table = {}
        i = 0
        i_words = []
        for w in s_words:
            if w not in table:
                table[w] = i
                i += 1
            i_words.append(table[w])
        print i_words

        def transf(s):
            if s not in table:
                return -1
            return table[s]

        L = len(words[0])
        for o in range(L):
            sec = []
            i = o
            while i <= len(s) - L:
                t = transf(s[i:i+L])
                if t < 0:
                    sec = []
                    i += L
                    continue
                if len(sec) == len(i_words):
                    sec.pop(0)
                sec.append(t)
                if len(sec) == len(i_words) and sorted(sec) == i_words:
                    res.append(i - (len(words) - 1) * L)
                i += L
        return res




s = Solution()

print s.findSubstring('barfoothefoobarman', ["foo", "bar"])
print s.findSubstring("wordgoodgoodgoodbestword", ["word","good","best","good"])
print datetime.now()-start
