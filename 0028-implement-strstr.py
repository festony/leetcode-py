#!/usr/bin/env python2
# https://leetcode.com/problems/implement-strstr/description/
from datetime import datetime
start = datetime.now()


class Solution(object):
    def strStr(self, haystack, needle):
        """
        :type haystack: str
        :type needle: str
        :rtype: int
        """
        # solution 1 - KMP - 49ms/25.98%
        # if needle == "":
        #     return 0
        # if len(haystack) < len(needle):
        #     return -1
        # if len(haystack) == len(needle):
        #     for i in range(len(haystack)):
        #         if haystack[i] != needle[i]:
        #             return -1
        #     return 0
        # # process needle and prepare shift table
        # shift = range(len(needle) + 1)
        # shift[0] = 1
        # for d in range(1, len(needle)):
        #     for i in range(len(needle) - d):
        #         if needle[i] != needle[d + i]:
        #             break
        #         if d < shift[i + d + 1]:
        #             shift[i + d + 1] = d
        # # print shift
        # text_start = 0
        # pattern_p = 0
        # while text_start <= len(haystack) - len(needle):
        #     if haystack[text_start + pattern_p] == needle[pattern_p]:
        #         pattern_p += 1
        #         if pattern_p == len(needle):
        #             # normal KMP:
        #             # print text_start
        #             # pattern_p -= shift[pattern_p]
        #             # text_start += shift[pattern_p]
        #             # easier than normal KMP: just break and return here
        #             return text_start
        #         continue
        #     text_start += shift[pattern_p]
        #     if pattern_p > 0:
        #         pattern_p -= shift[pattern_p]
        # return -1

        # solution 2 - boyer-moore - 65ms/13.21% hmmmmm....
        if needle == "":
            return 0
        if len(haystack) < len(needle):
            return -1
        if len(haystack) == len(needle):
            for i in range(len(haystack)):
                if haystack[i] != needle[i]:
                    return -1
            return 0

        shift = range(len(needle) + 1)[::-1]
        shift[-1] = 1
        for d in range(1, len(needle)):
            for i in range(len(needle) - d):
                if needle[-1-i] != needle[-1-d-i]:
                    break
                if d < shift[-2-d-i]:
                    shift[-2-d-i] = d
        # print shift

        cache = {}

        def get_bad_char_shift(c, p):
            if (c, p) in cache:
                return cache[(c, p)]
            for p1 in range(p)[::-1]:
                if needle[p1] == c:
                    cache[(c, p)] = p - p1
                    return p - p1
            cache[(c, p)] = p + 1
            return p + 1

        text_start = 0
        pattern_p = len(needle) - 1
        while text_start <= len(haystack) - len(needle):
            if haystack[text_start + pattern_p] == needle[pattern_p]:
                pattern_p -= 1
                if pattern_p < 0:
                    # normal boyer-moore:
                    # print text_start
                    # text_start += shift[pattern_p + 1]
                    # pattern_p = len(needle) - 1
                    # easier than normal Boyer-Moore: just break and return here
                    return text_start
                continue
            s1 = shift[pattern_p + 1]
            # TODO: here for good postfix shift: should be able to add bit checking to only check the latter x chars (as previous chars have already been compared)
            s2 = get_bad_char_shift(haystack[text_start + pattern_p], pattern_p)
            text_start += max(s1, s2)
            pattern_p = len(needle) - 1
        return -1







s = Solution()

print s.strStr('abcabcabca', 'abca')
print s.strStr('abcabcabca', 'aaabc')

print datetime.now()-start
