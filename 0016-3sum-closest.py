#!/usr/bin/env python2
# https://leetcode.com/problems/3sum/description/
from datetime import datetime
start=datetime.now()

class Solution(object):
    def threeSumClosest(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: int
        """
        # solution 1 time exceeded
        # nums.sort()
        #
        # def find_closest_index(t):
        #     low = 0
        #     up = len(nums)
        #     if t <= nums[low]:
        #         return low
        #     if t >= nums[up - 1]:
        #         return up - 2
        #     while up - low > 1:
        #         mid = (up + low) / 2
        #         if nums[mid] == t:
        #             return mid
        #         elif nums[mid] > t:
        #             up = mid
        #         else:
        #             low = mid
        #     if mid == len(nums) - 1:
        #         return mid - 1
        #     return mid
        #
        #
        # min_d = abs(target - (nums[0] + nums[1] + nums[2]))
        # ss = nums[0] + nums[1] + nums[2]
        # print min_d, ss
        # for i in range(len(nums) - 1):
        #     for j in range(i+1, len(nums)):
        #         s = nums[i] + nums[j]
        #         dif = target - s
        #         pos_left = find_closest_index(dif)
        #         pos_right = pos_left + 1
        #         while pos_left == i or pos_left == j:
        #             pos_left -= 1
        #         while pos_right == i or pos_right == j:
        #             pos_right += 1
        #         if pos_left < 0:
        #             pos_left = pos_right
        #         if pos_right > len(nums) - 1:
        #             pos_right = pos_left
        #
        #         gap1 = abs(dif - nums[pos_left])
        #         if gap1 < min_d:
        #             min_d = gap1
        #             ss = nums[i] + nums[j] + nums[pos_left]
        #             print i, j, pos_left
        #         gap2 = abs(dif - nums[pos_right])
        #         if gap2 < min_d:
        #             min_d = gap2
        #             ss = nums[i] + nums[j] + nums[pos_right]
        #             print i, j, pos_right
        # return ss

        # solution 2 bit DP? - very slow: 5.94%
        # nums.sort()
        # # print nums
        # def find_closest_index(t, area):
        #     low = area[0]
        #     up = area[1]
        #     if t <= nums[low]:
        #         return low
        #     if t >= nums[up - 1]:
        #         return up - 1
        #     while up - low > 1:
        #         mid = (up + low) / 2
        #         # print 'x', up, low, mid
        #         if nums[mid] == t:
        #             return mid
        #         elif nums[mid] > t:
        #             up = mid
        #         else:
        #             low = mid
        #     if low == len(nums) - 1:
        #         return low
        #     if abs(nums[low] - t) > abs(nums[low+1] - t):
        #         return low+1
        #     return low
        #
        # def find_closest_with_mid_index(i):
        #     # i > 0 and i < len - 1
        #     left_area = range(0, i)
        #     right_area = range(i+1, len(nums))
        #     t = target - nums[i]
        #     step_area = left_area
        #     bin_area = right_area
        #     # if len(left_area) > len(right_area):
        #     #     step_area, bin_area = bin_area, step_area
        #     d = t - nums[step_area[0]] - nums[bin_area[0]]
        #     # print 1, i, d
        #     if d <= 0:
        #         return target - d
        #     d = t - nums[step_area[-1]] - nums[bin_area[-1]]
        #     # print 2, i, d
        #     if d >= 0:
        #         return target - d
        #     # can do more optimization here
        #
        #     min_d = d
        #     min_d_abs = abs(d)
        #     for i1 in step_area:
        #         t2 = t - nums[i1]
        #         i2 = find_closest_index(t2, [bin_area[0], bin_area[-1]+1])
        #         # print i1, i, i2
        #         # print target, nums[i], t, nums[i1], t2, nums[i2], t2 - nums[i2]
        #         d = t2 - nums[i2]
        #         d_abs = abs(d)
        #         if min_d_abs > d_abs:
        #             min_d_abs = d_abs
        #             min_d = d
        #     return target - min_d
        #
        # closest_s = find_closest_with_mid_index(1)
        # min_d_abs = abs(target - closest_s)
        # for i in range(2, len(nums) - 1):
        #     s = find_closest_with_mid_index(i)
        #     # print s
        #     d_abs = abs(target - s)
        #     if d_abs < min_d_abs:
        #         closest_s = s
        #         min_d_abs = d_abs
        # return closest_s

        # solution 3 - slow, around 25%
        nums.sort()
        res = nums[0] + nums[1] + nums[2]
        for i in range(len(nums) - 2):
            j = i + 1
            k = len(nums) - 1
            while j < k:
                s = nums[i] + nums[j] + nums[k]
                if s == target:
                    return s
                if abs(target - s) < abs(target - res):
                    res = s
                if target - s > 0:
                    j += 1
                else:
                    k -= 1
        return res

















s = Solution()

# print s.threeSumClosest([-1, 0, 1, 2, -1, -4], 1)

# print s.threeSumClosest([-4,-2,-2,-2,0,1,2,2,2,3,3,4,4,6,6], 2)
# print s.threeSumClosest([-1, 2, 1, -4], 1)
# print s.threeSumClosest([1,1,-1], 2)
print s.threeSumClosest([-1,0,1,1,55], 3)

# print s.threeSumClosest([1,2,4,8,16,32,64,128], 82)
print datetime.now()-start
