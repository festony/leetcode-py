#!/usr/bin/env python2
# https://leetcode.com/problems/remove-element/description/
from datetime import datetime
start = datetime.now()


class Solution(object):
    def removeElement(self, nums, val):
        """
        :type nums: List[int]
        :type val: int
        :rtype: int
        """
        # solution 1 - 46ms/23.49%
        if nums == []:
            return 0
        p1 = 0
        p2 = len(nums) - 1
        while p2 >= 0 and nums[p2] == val:
            p2 -= 1
        if p2 < 0:
            return 0
        if p2 == 0:
            return 1
        while p1 < p2:
            if nums[p1] == val:
                nums[p1], nums[p2] = nums[p2], nums[p1]
                p2 -= 1
                while p2 >= 0 and nums[p2] == val:
                    p2 -= 1
                p1 += 1
            else:
                p1 += 1
        return p2 + 1

s = Solution()

print s.removeElement([2, 1, 1, 2], 2)

print datetime.now()-start
